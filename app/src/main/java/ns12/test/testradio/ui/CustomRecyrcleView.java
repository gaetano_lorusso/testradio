package ns12.test.testradio.ui;

import android.content.Context;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.animation.Interpolator;



public class CustomRecyrcleView extends RecyclerView {
    private static final int POW = 2000;

    private Interpolator interpolator;

    public CustomRecyrcleView(Context context) {
        super(context);
        createInterpolator();
    }

    public CustomRecyrcleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createInterpolator();
    }

    public CustomRecyrcleView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        createInterpolator();
    }

    private void createInterpolator(){
        interpolator = new Interpolator() {
            @Override
            public float getInterpolation(float t) {
                t = Math.abs(t - 1.0f);
                return (float) (1.0f - Math.pow(t, POW));
            }
        };
    }

    @Override
    public void smoothScrollBy(int dx, int dy) {
        super.smoothScrollBy(dx, dy, interpolator);
    }
}
