package ns12.test.testradio.ui;

import android.app.DialogFragment;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import ns12.test.testradio.R;
import ns12.test.testradio.music_recognition.GetProductLink;
import ns12.test.testradio.ui.ui_interfaces.CommercialButtonListner;
import ns12.test.testradio.util.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class CommercialButton extends android.support.v4.app.DialogFragment
{

    private TextView nameProduct;
    private ImageView carrello;
    private Button button;
    private CommercialButtonListner commercialButtonListner;
    private ImageView close;
    private CardView cardOnDemand  ;
    private  String artistName;
    private String codiceProdotto;
    private String codiceRadio;
    private String webLink = "https://www.radionovelli.it/catalogo/";

    public static CommercialButton getInstance(String artistname,String nameProduct, String codiceRadio, int  height,CommercialButtonListner listner){
        Bundle args = new Bundle();
        args.putString(Constants.KEY_ARTIST_NAME,artistname);
        args.putString(Constants.KEY_PRODUCT,nameProduct);
        args.putString(Constants.KEY_CODICE_RADIO, codiceRadio);
        args.putInt(Constants.KEY_HEIGHT_TOOLBAR,height);
        CommercialButton commercialButton = new CommercialButton();
        commercialButton.setCommercialButtonListner(listner);
        commercialButton.setStyle(DialogFragment.STYLE_NORMAL,  R.style.BuyNowDialog);
        commercialButton.setArguments(args);;
        return  commercialButton;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        codiceProdotto = getArguments().getString(Constants.KEY_PRODUCT);
        codiceRadio = getArguments().getString(Constants.KEY_CODICE_RADIO);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://31.169.104.171/")
                .client(new OkHttpClient.Builder().build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        GetProductLink service = retrofit.create(GetProductLink.class);
        Call<String> call = service.getProductFromCode(codiceProdotto, codiceRadio);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.body() != null){
                    webLink = response.body();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.commercialbutton, container, false);
        final Bundle arg=getArguments();
        Rect rectangle = new Rect();
        Window window = getActivity().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop =window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight= contentViewTop - statusBarHeight;

        Window window2 = getActivity().getWindow();
        Point size = new Point();
        Display display = window2.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        int newHeight=height-(-titleBarHeight+arg.getInt(Constants.KEY_HEIGHT_TOOLBAR));
        getDialog().getWindow().setLayout((int) (width * 0.3), newHeight);
        getDialog().getWindow().setGravity(Gravity.START | Gravity.BOTTOM);
        artistName=arg.getString(Constants.KEY_ARTIST_NAME);
        carrello = view.findViewById(R.id.carrello);
        cardOnDemand=view.findViewById(R.id.cardOnDemand);
        Glide
                .with(getActivity())
                .load("https://www.passionetecnologica.it/Sito/uploads/edd/2014/02/ecommerce.jpg")
                .apply(new RequestOptions().override(250, 250))
                .into(carrello);
        ConstraintLayout constraintLayout = view.findViewById(R.id.layout);
        constraintLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.music_info_dialog));
        button = view.findViewById(R.id.compraqui);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                commercialButtonListner.showWebView(webLink);
                dismiss();
            }
        });
        nameProduct=view.findViewById(R.id.nameProduct);
        nameProduct.setText(arg.getString(Constants.KEY_PRODUCT));
        close=view.findViewById(R.id.button_close);
        close.setBackgroundResource(R.drawable.ic_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        cardOnDemand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commercialButtonListner.openYoutube(artistName);
            }
        });

        return view;
    }

    public void setCommercialButtonListner(CommercialButtonListner commercialButtonListner) {
        this.commercialButtonListner = commercialButtonListner;

    }


    public void setInfo(String artist,String prodotto){
        //nameProduct.setText(prodotto);
        artistName=artist;
    }
}