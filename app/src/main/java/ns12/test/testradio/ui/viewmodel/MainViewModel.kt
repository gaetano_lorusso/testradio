package ns12.test.testradio.ui.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ns12.test.testradio.R
import ns12.test.testradio.Repository.RadioDatabase
import ns12.test.testradio.Rtl_sdr.IQSourceInterface
import ns12.test.testradio.model.Radio
import ns12.test.testradio.rtl_sdr.*
import ns12.test.testradio.util.Constants
import ns12.test.testradio.util.NetworkUtil
import ns12.test.testradio.util.ioThread
import ns12.test.testradio.youtube_player.Search_YouTube
import ns12.test.testradio.youtube_player.YoutubeSearchResult
import ns12.test.testradio.youtube_player.YoutubeVideoModel
import java.util.*



/**
 * Created by userns12 on 12/13/17.
 */
    class MainViewModel(val context: Application) : MainContract.ViewModel, IQSourceInterface.Callback, RFControlInterface, AndroidViewModel(context), YoutubeSearchResult {





    val authLive: MutableLiveData<Boolean> = MutableLiveData()
    val radioList: MutableLiveData<ArrayList<Radio>> = MutableLiveData<ArrayList<Radio>>()
    val frequency: MutableLiveData<Long> = MutableLiveData()
    val currentSource: MutableLiveData<SourceType> = MutableLiveData()
    val currentPlayerStatus: MutableLiveData<PlayerState> = MutableLiveData()
    private val LOGTAG = "RADIO FM"

    private var isPlaying: Boolean = false
    var radioArray = ArrayList<Radio>()
    var currentSourceType: SourceType
    var currentCity: String
    private var analyzerSurface: AnalyzerSurface
    private var analyzerProcessingLoop: AnalyzerProcessingLoop? = null
    private var source: IQSourceInterface? = null
    private var scheduler: Scheduler? = null
    private var demodulator: Demodulator? = null
    private var demodulationMode: Int? = Demodulator.DEMODULATION_WFM
    var currentRadio: Radio? = null
    var currentFrequency: Long? = null
    var isRecognizing: Boolean = false
    var videoList: MutableLiveData<ArrayList<YoutubeVideoModel>> = MutableLiveData()
    var isProgress: MutableLiveData<String> = MutableLiveData()


    fun searchArtistOnYoutube(artist: String) {
        isProgress.postValue("start")
        val apiKey = context.getString(R.string.youtube_api_key)
        ioThread {
            Search_YouTube.getYouTubeSearch(artist, apiKey, this);
        }

    }


    override fun onSuccess(videoIds: ArrayList<YoutubeVideoModel>) {

        isProgress.postValue("success")
        videoList.postValue(videoIds)
    }

    override fun onError() {
        isProgress.postValue("error")
    }

    init {

        currentSourceType = selectSourceFromPref(context.getSharedPreferences(Constants.KEY_SOURCE_SETTINGS, 0).getString(Constants.KEY_SOURCE_RADIO, Constants.VALUE_SETTINGS_WEB))
        currentCity = context.getSharedPreferences(Constants.KEY_CITY_SETTINGS, 0).getString(Constants.KEY_CITY, Constants.VALUE_DEFAULT_CITY)

        when (currentSourceType) {
            SourceType.WEB -> {
                var status = NetworkUtil.getConnectivityStatusString(context);
                if (status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                    //currentSourceType = SourceType.FM

                }
            }
            SourceType.FM -> {

            }
        }
        currentSource.postValue(currentSourceType)
        analyzerSurface = AnalyzerSurface(context, this)




    }



    fun checkIsAuth() {
        authLive.postValue(FirebaseAuth.getInstance().getCurrentUser() != null)
    }



    override fun loadRadioList() {
       ioThread {
           checkVersion()
           val radioEntities = RadioDatabase.getInstance(context.applicationContext).radioDao().getAllRadio()
           for (radioEntity in radioEntities) {
               radioArray.add(Radio(radioEntity.name, radioEntity.web, radioEntity.imageurl, radioEntity.codice))

           }
           changeCity(currentCity)
       }
    }

    private fun checkVersion() {
        val database = FirebaseDatabase.getInstance()
        val refVersion = database.getReference("/version")
        refVersion.addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {

            }
        })
    }


    override fun getSourceType(): SourceType {
        return currentSourceType
    }


    override fun onDestroy() {
    }


    override fun selectSource(sourceType: SourceType) {
        if (!currentSourceType.equals(sourceType)) {
            currentSourceType = sourceType
            onSourceChanged()
        }
    }

    fun onSourceChanged() {
        when (currentSourceType) {
            SourceType.WEB -> {
                var status = NetworkUtil.getConnectivityStatusString(context);
                if (status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                   // currentSourceType = SourceType.FM
                } else {
                    stopAnalyzer()
                    source = null
                    isPlaying = true;
                    currentSource.postValue(currentSourceType)
                }
            }
            SourceType.FM -> {
                currentSource.postValue(currentSourceType)
                if (isPlaying) {
                    startAnalyzer()
                }
            }
            SourceType.DAB -> {
                currentSource.postValue(currentSourceType)

            }

        }
    }

    override fun onMediaBrowserConnected() {
        if (isPlaying) {

            currentPlayerStatus.postValue(PlayerState.PLAY)
        }
    }

    private fun selectSourceFromPref(sourceValue: String): SourceType {

        when (sourceValue) {
            "WEB" -> return SourceType.WEB
            "FM" -> return SourceType.FM
            "DAB" -> return SourceType.DAB
            else -> return SourceType.WEB
        }
    }


    /**
     * Will start the RF Analyzer. This includes creating a source (if null), open a source
     * (if not open), starting the scheduler (which starts the source) and starting the
     * processing loop.
     */
    fun startAnalyzer() {
        this.stopAnalyzer()    // Stop if runningRadioFm; This assures that we don't end up with multiple instances of the thread loops

        // Retrieve fft size and frame rate from the preferences
        val fftSize = Integer.valueOf("1024")
        val frameRate = Integer.valueOf("1")
        val dynamicFrameRate = true

        isPlaying = true

        if (source == null) {
            if (!this.createSource())
                return
        }

        // check if the source is open. if not, open it!
        if (!source!!.isOpen()) {

            if (source!!.open(getApplication(), this)) {
                // Toast.makeText(context, "Source not available (" + source!!.getName() + ")", Toast.LENGTH_LONG).show()
                isPlaying = false
                return
            }
            return    // we have to wait for the source to become ready... onIQSourceReady() will call startAnalyzer() again...
        }

        // Create a new instance of Scheduler and Processing Loop:
        scheduler = Scheduler(fftSize, source!!)
        analyzerProcessingLoop = AnalyzerProcessingLoop(analyzerSurface,

                fftSize, // FFT size
                scheduler!!.getFftOutputQueue(), // Reference to the input queue for the processing loop
                scheduler!!.getFftInputQueue()) // Reference to the buffer-pool-return queue
        if (dynamicFrameRate)
            analyzerProcessingLoop!!.setDynamicFrameRate(true)
        else {
            analyzerProcessingLoop!!.setDynamicFrameRate(false)
            analyzerProcessingLoop!!.setFrameRate(frameRate)
        }

        // Start both threads:
        scheduler!!.start()
        analyzerProcessingLoop!!.start()

        currentFrequency = currentRadio!!.frequency
        frequency.postValue(currentFrequency)
        scheduler!!.channelFrequency = currentFrequency!!
        analyzerSurface.virtualFrequency = currentFrequency!!
        // Start the demodulator thread:
        demodulator = Demodulator(scheduler!!.getDemodOutputQueue(), scheduler!!.getDemodInputQueue(), source!!.getPacketSize())
        demodulator!!.start()

        // Set the demodulation mode (will configure the demodulator correctly)
        this.setDemodulationMode(demodulationMode!!)

        // Prevent the screen from turning off:
        // this.runOnUiThread(java.lang.Runnable { getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) })
    }

    /**
     * Will create a IQ Source instance according to the user settings.
     *
     * @return true on success; false on error
     */
    fun createSource(): Boolean {
        val frequency: Long
        var sampleRate: Int

        // Create RtlsdrSource

        source = RtlsdrSource("127.0.0.1", 1234)

        frequency = 97000000
        sampleRate = 2000000
        source!!.setFrequency(frequency)
        source!!.setSampleRate(sampleRate)

        (source as RtlsdrSource).setFrequencyCorrection(0)
        (source as RtlsdrSource).setFrequencyOffset(0)
        (source as RtlsdrSource).setManualGain(false)
        (source as RtlsdrSource).setAutomaticGainControl(true)
        if ((source as RtlsdrSource).isManualGain()) {
            (source as RtlsdrSource).setGain(0)
            (source as RtlsdrSource).setIFGain(0)
        }


        // inform the analyzer surface about the new source
        analyzerSurface.setSource(source)

        return true
    }


    /**
     * Called by the analyzer surface after the user changed the channel width
     * @param newChannelWidth    new channel width (single sided) in Hz
     * @return true if channel width is valid; false if out of range
     */
    override fun updateChannelWidth(newChannelWidth: Int): Boolean {
        if (demodulator != null) {
            if (demodulator!!.setChannelWidth(newChannelWidth)) {
                analyzerSurface!!.setChannelWidth(newChannelWidth)
                return true
            }
        }
        return false
    }

    override fun updateChannelFrequency(newChannelFrequency: Long): Boolean {
        if (scheduler != null) {
            scheduler!!.setChannelFrequency(newChannelFrequency)
            analyzerSurface.setChannelFrequency(newChannelFrequency)
            return true
        }
        return false
    }

    fun updateFrequency(stato: Boolean, radio: Radio) {
        var freq = radio!!.frequency
        var offSet: Long = 10000
        if (stato) {
           // if (currentFrequency!! < freq.plus(100000))
                currentFrequency = currentFrequency!!.plus(offSet)
        } else {
            //if (currentFrequency!! > freq.minus(100000))
                currentFrequency = currentFrequency!!.minus(offSet)
        }

        frequency.postValue(currentFrequency)
        updateChannelFrequency(currentFrequency!!)
    }


    override fun updateSourceFrequency(newSourceFrequency: Long): Boolean {
        if (source != null && newSourceFrequency <= source!!.getMaxFrequency()
                && newSourceFrequency >= source!!.getMinFrequency()) {
            source!!.setFrequency(newSourceFrequency)
            analyzerSurface.setVirtualFrequency(newSourceFrequency)
            return true
        }
        return false
    }

    override fun updateSampleRate(newSampleRate: Int): Boolean {
        if (source != null) {
            if (scheduler == null || !scheduler!!.isRecording()) {
                source!!.setSampleRate(newSampleRate)
                return true
            }
        }
        return false
    }

    override fun updateSquelch(newSquelch: Float) {
        analyzerSurface.setSquelch(newSquelch)
    }

    override fun updateSquelchSatisfied(squelchSatisfied: Boolean): Boolean {
        if (scheduler != null) {
            scheduler!!.setSquelchSatisfied(squelchSatisfied)
            return true
        }
        return false
    }

    override fun requestCurrentChannelWidth(): Int {
        return if (demodulator != null)
            demodulator!!.getChannelWidth()
        else
            -1
    }

    override fun requestCurrentChannelFrequency(): Long {
        return if (scheduler != null)
            scheduler!!.getChannelFrequency()
        else
            -1
    }

    override fun requestCurrentDemodulationMode(): Int {
        return demodulationMode!!
    }

    override fun requestCurrentSquelch(): Float {
        return if (analyzerSurface != null)
            analyzerSurface.getSquelch()
        else
            java.lang.Float.NaN
    }

    override fun requestCurrentSourceFrequency(): Long {
        return if (source != null)
            source!!.getFrequency()
        else
            -1
    }

    override fun requestCurrentSampleRate(): Int {
        return if (source != null)
            source!!.getSampleRate()
        else
            -1
    }

    override fun requestMaxSourceFrequency(): Long {
        return if (source != null)
            source!!.getMaxFrequency()
        else
            -1
    }

    override fun requestSupportedSampleRates(): IntArray? {
        return if (source != null)
            source!!.getSupportedSampleRates()
        else
            null
    }

    override fun updateDemodulationMode(newDemodulationMode: Int): Boolean {
        if (scheduler == null || demodulator == null || source == null) {
            Log.e(LOGTAG, "updateDemodulationMode: scheduler/demodulator/source is null (no demodulation running)")
            return false
        }

        setDemodulationMode(newDemodulationMode)
        return true
    }


    /**
     * Will set the modulation mode to the given value. Takes care of adjusting the
     * scheduler and the demodulator respectively and updates the action bar menu item.
     *
     * @param mode    Demodulator.DEMODULATION_OFF, *_AM, *_NFM, *_WFM
     */
    fun setDemodulationMode(mode: Int) {
        var mode = mode
        if (scheduler == null || demodulator == null || source == null) {
            Log.e(LOGTAG, "setDemodulationMode: scheduler/demodulator/source is null")
            return
        }

        // (de-)activate demodulation in the scheduler and set the sample rate accordingly:

        // adjust sample rate of the source:
        source!!.setSampleRate(Demodulator.INPUT_RATE)

        // Verify that the source supports the sample rate:
        if (source!!.getSampleRate() !== Demodulator.INPUT_RATE) {
            Log.e(LOGTAG, "setDemodulationMode: cannot adjust source sample rate!")
            Toast.makeText(context, "Source does not support the sample rate necessary for demodulation (" +
                    Demodulator.INPUT_RATE / 1000000 + " Msps)", Toast.LENGTH_LONG).show()
            scheduler!!.setDemodulationActivated(false)
            mode = Demodulator.DEMODULATION_OFF    // deactivate demodulation...
        } else {
            scheduler!!.setDemodulationActivated(true)
        }

        // set demodulation mode in demodulator:
        demodulator!!.setDemodulationMode(mode)
        this.demodulationMode = mode    // save the setting

        analyzerSurface.setDemodulationEnabled(true)
    }


    /**
     * Will open the IQ Source instance.
     * Note: some sources need special treatment on opening, like the rtl-sdr source.
     *
     * @return true on success; false on error
     */
    fun openSource(): Boolean {

        if (source != null && source is RtlsdrSource) {
            // We might need to start the driver:

            // start local rtl_tcp instance:
            try {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setClassName("marto.rtl_tcp_andro", "com.sdrtouch.rtlsdr.DeviceOpenActivity")
                intent.data = Uri.parse("iqsrc://-a 127.0.0.1 -p 1234 -n 1")
                // (view as MainActivity).startActivityForResult(intent, RTL2832U_RESULT_CODE)
            } catch (e: ActivityNotFoundException) {
                Log.e(LOGTAG, "createSource: RTL2832U is not installed")

                // Show a dialog that links to the play market:
                /*  AlertDialog.Builder(context)
                          .setTitle("RTL2832U driver not installed!")
                          .setMessage("You need to install the (free) RTL2832U driver to use RTL-SDR dongles.")
                          .setPositiveButton("Install from Google Play") { dialog, whichButton ->
                              val marketIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=marto.rtl_tcp_andro"))
                              startActivity(marketIntent)
                          }
                          .setNegativeButton("Cancel") { dialog, whichButton ->
                              // do nothing
                          }
                          .show()*/
                return false
            }

            return source!!.open(context, this)
        } else {
            Log.e(LOGTAG, "openSource: sourceType is RTLSDR_SOURCE, but source is null or of other type.")
            return false
        }


    }


    /**
     * Will stop the RF Analyzer. This includes shutting down the scheduler (which turns of the
     * source), the processing loop and the demodulator if runningRadioFm.
     */
    fun stopAnalyzer() {
        // Stop the Scheduler if runningRadioFm:

        // Stop recording in case it is runningRadioFm:

        scheduler?.stopScheduler()


        // Stop the Processing Loop if runningRadioFm:

        analyzerProcessingLoop?.stopLoop()

        // Stop the Demodulator if runningRadioFm:

        demodulator?.stopDemodulator()

        // Wait for the scheduler to stop:
        if (!scheduler?.getName().equals(Thread.currentThread().name)) {
            try {
                scheduler?.join()
            } catch (e: Throwable) {
                Log.e(LOGTAG, "startAnalyzer: Error while stopping Scheduler.")
            }

        }

        // Wait for the processing loop to stop
        try {
            analyzerProcessingLoop?.join()
        } catch (e: InterruptedException) {
            Log.e(LOGTAG, "startAnalyzer: Error while stopping Processing Loop.")
        }

        // Wait for the demodulator to stop
        try {
            demodulator?.join()
        } catch (e: InterruptedException) {
            Log.e(LOGTAG, "startAnalyzer: Error while stopping Demodulator.")
        }



        isPlaying = false

        // allow screen to turn off again:
        // this.runOnUiThread(java.lang.Runnable { getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) })
    }


    override fun onIQSourceReady(source: IQSourceInterface) {
        startAnalyzer()
    }

    override fun onIQSourceError(source: IQSourceInterface, message: String) {
        //   (view as MainActivity).runOnUiThread(java.lang.Runnable { Toast.makeText(context, "Error with Source [" + source.getName() + "]: " + message, Toast.LENGTH_LONG).show() })
        stopAnalyzer()

        if (source.isOpen())
            this.source?.close()

    }


    override fun onIQSoureReload(source: IQSourceInterface, message: String) {
        stopAnalyzer()

        if (source.isOpen())
            this.source?.close()

        startAnalyzer()

    }

    override fun manageDriverError() {
        source?.close()
    }


    override fun play() {

        isPlaying = true
        //recognizeMusic()
        //recognizeCommercial(currentRadio!!.url)

        when (currentSourceType) {
            SourceType.FM -> startAnalyzer()
        // SourceType.WEB -> currentPlayerStatus.postValue(PlayerState.PLAY)
        }
    }

    override fun pause() {
        isPlaying = false
        //stopRecognizing()
        when (currentSourceType) {
            SourceType.FM -> stopAnalyzer()
            SourceType.WEB -> currentPlayerStatus.postValue(PlayerState.PAUSE)
        }
    }

    override fun onRadioSelected(selectedRadio: Radio) {
        currentRadio = selectedRadio

    }

    fun changeCity(citta: String) {
        currentCity = citta
        val frequenceEntitys = RadioDatabase.getInstance(context.applicationContext).radioDao().getFrequencyByCity(currentCity)

        for (frequenceEntity in frequenceEntitys) {
            radioArray.get(frequenceEntity.radioid - 1)?.setFrequency(frequenceEntity.frequency)
        }

        if(radioArray.size >0)
            radioList.postValue(radioArray)

    }





}