package ns12.test.testradio.ui.viewmodel;

/**
 * Created by userns12 on 1/5/18.
 */

public enum PlayerState {

    PLAY, PAUSE
}
