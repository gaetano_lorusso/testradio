package ns12.test.testradio.ui

import android.content.DialogInterface
import android.graphics.SurfaceTexture
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.*
import kotlinx.android.synthetic.main.video_fragment.*
import ns12.test.testradio.R



class VideoFragment : DialogFragment(), TextureView.SurfaceTextureListener {

    var _introMediaPlayer : MediaPlayer? = null


    override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture?, p1: Int, p2: Int) {
    }

    override fun onSurfaceTextureUpdated(p0: SurfaceTexture?) {
    }

    override fun onSurfaceTextureDestroyed(p0: SurfaceTexture?): Boolean {
        return false
    }

    override fun onSurfaceTextureAvailable(p0: SurfaceTexture?, p1: Int, p2: Int) {
        try {
            destoryIntroVideo()

            _introMediaPlayer = MediaPlayer.create(activity, R.raw.video_novelli)
            _introMediaPlayer!!.setSurface(Surface(p0))
            _introMediaPlayer!!.setLooping(true)
            _introMediaPlayer!!.setOnPreparedListener{ mediaPlayer -> mediaPlayer.start() }

        } catch (e: Exception) {
            System.err.println("Error playing intro video: " + e.message)
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.video_fragment, container,false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       video.surfaceTextureListener = this


    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
    }

    override fun onDestroy() {
        super.onDestroy()
        destoryIntroVideo()
    }

    private fun destoryIntroVideo() {
        if (_introMediaPlayer != null) {
            _introMediaPlayer!!.stop()
            _introMediaPlayer!!.release()
            _introMediaPlayer = null
        }
    }
}