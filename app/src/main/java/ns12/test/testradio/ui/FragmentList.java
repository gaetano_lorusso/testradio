package ns12.test.testradio.ui;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;

import java.util.ArrayList;
import java.util.List;

import ns12.test.testradio.R;
import ns12.test.testradio.model.Radio;
import ns12.test.testradio.ui.ui_interfaces.SourceListener;
import ns12.test.testradio.ui.viewmodel.MainViewModel;
import ns12.test.testradio.ui.viewmodel.SourceType;



public class FragmentList extends android.support.v4.app.Fragment{

    private List<Radio> radio;
    private RecyclerView recyclerView;
    private ns12.test.testradio.ui.CardRadioAdapter cardRadioAdapter;
    private View view;
    private MainActivity activity;
    public static int visibleItem;
    public static int lastClickedItem;
    private CarouselLayoutManager carouselLayoutManager;
    private Button dabButton;
    private Button webButton;
    private Button fmButton;
    private Button autoButton;
    private SourceListener sourceListener;
    private SourceType sourceType ;
    private ImageView plus;
    private ImageView meno;
    private FrameLayout frameLayout;
    private MainViewModel mainViewModel;


    public static FragmentList getInstance(String radio){
        Bundle args = new Bundle();
        //args.putString(Constants.KEY_ARGS_LABEL_RADIO,radio);
        FragmentList fragmentList = new FragmentList();

        return  fragmentList;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup vg,
                             Bundle savedInstanceState) {


        view=inflater.inflate(R.layout.fragment_list, vg, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mainViewModel.loadRadioList();
        mainViewModel.getRadioList().observe(this, new Observer<ArrayList<Radio>>() {

            @Override
            public void onChanged(@Nullable ArrayList<Radio> radios) {
                startAdapter(radios);
            }
        });
    }

    public void startAdapter(List<Radio> radioList){
        radio = radioList;
        recyclerView =  view.findViewById(R.id.recycler);
        carouselLayoutManager= new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL);
        carouselLayoutManager.setPostLayoutListener(new  CarouselZoomPostLayoutListener());
        carouselLayoutManager.setMaxVisibleItems(3);
        recyclerView.setLayoutManager(carouselLayoutManager);
        recyclerView.setHasFixedSize(true);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);


        cardRadioAdapter = new ns12.test.testradio.ui.CardRadioAdapter(radio,getActivity(),this);
        carouselLayoutManager.addOnItemSelectionListener(new CarouselLayoutManager.OnCenterItemSelectionListener() {
            @Override
            public void onCenterItemChanged(int adapterPosition) {
                if (radio.size()!=0){
                    visibleItem=adapterPosition;
                    sourceListener.onRadioSelected(radio.get(adapterPosition));
                }
            }
        });

        recyclerView.setAdapter(cardRadioAdapter);
        recyclerView.scrollToPosition(cardRadioAdapter.getItemCount()/2);
        visibleItem=cardRadioAdapter.getItemCount()/2;
        lastClickedItem=cardRadioAdapter.getItemCount()/2;
        dabButton = view.findViewById(R.id.dab_button);
        dabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sourceType!=SourceType.DAB){
                    setColor(SourceType.DAB);
                    dabButton.startAnimation(AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.click));
                    sourceListener.changeSourceListener(SourceType.DAB);
                    sourceType=SourceType.DAB;
                }


            }
        });

        fmButton=view.findViewById(R.id.fm_button);
        fmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sourceType!=SourceType.FM){
                    fmButton.startAnimation(AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.click));
                    setColor(SourceType.FM);
                    sourceListener.changeSourceListener(SourceType.FM);
                    sourceType=SourceType.FM;
                }



            }
        });
        webButton=view.findViewById(R.id.web_button);
        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sourceType!=SourceType.WEB){
                    webButton.startAnimation(AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.click));
                    setColor(SourceType.WEB);
                    sourceListener.changeSourceListener(SourceType.WEB);
                    sourceType=SourceType.WEB;
                }

            }
        });
        /*plus=view.findViewById(R.id.plus);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                plus.startAnimation(AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.click));
                sourceListener.updateFrequency(true);

            }
        });
        meno=view.findViewById(R.id.meno);
        meno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                meno.startAnimation(AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.click));
                sourceListener.updateFrequency(false);

            }
        });*/


        setColor(sourceType);



    }

    public int getVisibleItem(){

        return visibleItem;
    }
    public static  void setVisibleItem(Integer x){
        visibleItem=x;
    }
    public String getRadioUrl(){

        return radio.get(getVisibleItem()).getUrl();
    }


    public Radio getSelectedRadio(){
        return radio.get(visibleItem);
    }

    public String getRadioName(){

        return radio.get(getVisibleItem()).getNome();
    }

    public static void setLastClickedItem(int i){
        lastClickedItem=i;
    }

    public static int lastClickItem(){

        return lastClickedItem;
    }


    public void setSourceListener(SourceListener sourceListener){
        this.sourceListener=sourceListener;
    }

    public void setClickable( boolean clickable){
//        autoButton.setClickable(clickable);
        if(fmButton!= null && webButton!=null && dabButton !=null){
            fmButton.setClickable(clickable);
            webButton.setClickable(clickable);
            dabButton.setClickable(clickable);
        }


    }

    public void setSourceSelected(SourceType sourceType){
        this.sourceType=sourceType;
        if ( webButton!= null && fmButton!=null && dabButton!= null){
            setColor(this.sourceType);
        }

    }

    public void setVisibleControlForFrequency(boolean stato){
        if (plus!=null && meno!=null){
            if (stato){
                plus.setVisibility(View.VISIBLE);
                meno.setVisibility(View.VISIBLE);
            }else{
                plus.setVisibility(View.INVISIBLE);
                meno.setVisibility(View.INVISIBLE);
            }
        }

    }


    public void setColor(SourceType sourceType){
       if(sourceType==SourceType.WEB){
           webButton.setTextColor(Color.RED);
           dabButton.setTextColor(Color.BLACK);
           fmButton.setTextColor(Color.BLACK);
//           autoButton.setTextColor(Color.BLACK);
       }
        if(sourceType==SourceType.FM){
            fmButton.setTextColor(Color.RED);
            dabButton.setTextColor(Color.BLACK);
            webButton.setTextColor(Color.BLACK);
//            autoButton.setTextColor(Color.BLACK);

        }
        if(sourceType==SourceType.DAB){
            dabButton.setTextColor(Color.RED);
            webButton.setTextColor(Color.BLACK);
            fmButton.setTextColor(Color.BLACK);
//            autoButton.setTextColor(Color.BLACK);
        }
    }


    public boolean selectVocalRadio(String result){
        for (int i = 0; i < cardRadioAdapter.getItemCount(); i++){
            if (cardRadioAdapter.getItem(i).getNome().equalsIgnoreCase(result)){
                recyclerView.smoothScrollToPosition(i);
                return true;
            }
        }
        return false;
    }














}

