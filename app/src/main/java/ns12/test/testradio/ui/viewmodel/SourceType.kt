package ns12.test.testradio.ui.viewmodel

/**
 * Created by userns12 on 12/18/17.
 */
enum class SourceType {

    WEB,
    FM,
    DAB,
    NULL
}