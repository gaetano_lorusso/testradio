package ns12.test.testradio.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.share.Share;

import java.util.prefs.Preferences;

import ns12.test.testradio.R;
import ns12.test.testradio.util.Constants;



    public  class MyPreferenceFragment extends PreferenceFragment
    {
        private View view;
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);
            final SharedPreferences sourceSettings = getActivity().getSharedPreferences(Constants.KEY_SOURCE_SETTINGS, 0);
            final SharedPreferences citySetting= getActivity().getSharedPreferences(Constants.KEY_CITY_SETTINGS, 0);
            final SharedPreferences youtubeSetting=getActivity().getSharedPreferences(Constants.KEY_YOUTUBE_SETTINGS,0);
            final Preference sourcePreference = findPreference(getString(R.string.key_source_radio));
            final Preference cityPreference = findPreference(getString(R.string.key_city));
            final Preference youtubePreference =findPreference(getString(R.string.key_youtube));

            cityPreference.setSummary(citySetting.getString(Constants.KEY_CITY,Constants.VALUE_DEFAULT_CITY));
            cityPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    SharedPreferences.Editor editor = citySetting.edit();
                    editor.putString(getString(R.string.key_city),o.toString());
                    editor.commit();
                    cityPreference.setSummary(o.toString());
                    advertiseChangeCity(o.toString());
                    return true;
                }
            });

            sourcePreference.setSummary(sourceSettings.getString(Constants.KEY_SOURCE_RADIO,Constants.VALUE_SETTINGS_WEB));


            sourcePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    SharedPreferences.Editor editor = sourceSettings.edit();
                    editor.putString(getString(R.string.key_source_radio),o.toString());
                    editor.commit();
                    sourcePreference.setSummary(o.toString());

                    return true;
                }
            });
            youtubePreference.setSummary(youtubeSetting.getString(getString(R.string.key_youtube),Constants.VALUE_SETTINGS_YOUTUBE));

            youtubePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    SharedPreferences.Editor editor = youtubeSetting.edit();
                    editor.putString(getString(R.string.key_youtube), o.toString());
                    editor.commit();
                    youtubePreference.setSummary(o.toString());

                    return true;
                }
            });
        }


        public View onCreateView(LayoutInflater inflater, ViewGroup vg,Bundle savedInstanceState) {
            view=super.onCreateView(inflater, vg, savedInstanceState);
            view.setBackgroundColor(getResources().getColor(R.color.preferences));
            return view;
        }


        public void advertiseChangeCity(String citta){

            MainActivity mainActivity=(MainActivity) getActivity();
            mainActivity.changeCity(citta);
        }






    }




