package ns12.test.testradio.ui.ui_interfaces;

import ns12.test.testradio.model.Radio;
import ns12.test.testradio.ui.viewmodel.SourceType;



public interface SourceListener {

    void changeSourceListener(SourceType sourceType);

    void onRadioSelected(Radio radio);

    void updateFrequency(boolean stato);

    Radio getSelectedRadio();
    Radio getCurrentRadio() ;
    void play();

    void startVoiceRecognition();
}
