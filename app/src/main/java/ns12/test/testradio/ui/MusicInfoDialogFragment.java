package ns12.test.testradio.ui;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import ns12.test.testradio.R;
import ns12.test.testradio.ui.ui_interfaces.MusicInfoListener;
import ns12.test.testradio.util.Constants;


public class MusicInfoDialogFragment extends android.support.v4.app.DialogFragment {

    private String song;
    private String artist;
    private TextView songName;
    private TextView artistName;
    private ImageView imageView;
    private ImageView youtube;
    private PackageManager manager;
    private MusicInfoListener musicInfoListener;
    private ImageView close;

    public static MusicInfoDialogFragment getInstance(String song, String artist, String imageUrl, int height) {
        Bundle args = new Bundle();
        args.putString(Constants.KEY_IMAGE_URL_ARTIST, imageUrl);
        args.putString(Constants.KEY_SONG_NAME, song);
        args.putString(Constants.KEY_ARTIST_NAME, artist);
        args.putInt(Constants.KEY_HEIGHT_TOOLBAR, height);
        MusicInfoDialogFragment musicInfoDialogFragment = new MusicInfoDialogFragment();
        musicInfoDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.PauseDialog);
        musicInfoDialogFragment.setArguments(args);
        return musicInfoDialogFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle arg = getArguments();
            /*View view = inflater.inflate(R.layout.dialog_music_info, container, false);

            Rect rectangle = new Rect();
            Window window = getActivity().getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
            int statusBarHeight = rectangle.top;
            int contentViewTop =window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
            int titleBarHeight= contentViewTop - statusBarHeight;

            Window window2 = getActivity().getWindow();
            Point size = new Point();
            Display display = window2.getWindowManager().getDefaultDisplay();
            display.getSize(size);int width = size.x;
            int height = size.y;*/

        View view = View.inflate(getContext(), R.layout.dialog_music_info, null);
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        int maxHeight = (int) (height * 0.7);
        int maxwidth = (int) (width * 0.6);
        getDialog().getWindow().setLayout(maxHeight, maxwidth);
        getDialog().getWindow().setGravity(Gravity.TOP | Gravity.CENTER_VERTICAL);
        getDialog().setCanceledOnTouchOutside(false);

        artist = arg.getString(Constants.KEY_ARTIST_NAME);
        song = arg.getString(Constants.KEY_SONG_NAME);
        songName = view.findViewById(R.id.song_title);
        artistName = view.findViewById(R.id.artist_name);
        imageView = view.findViewById(R.id.artist_image);
        youtube = view.findViewById(R.id.youtube);
        close=view.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                musicInfoListener.openYoutube(artist);
                // musicInfoListener.closeMusicPlayer();
                dismiss();
            }
        });


        loadApps();

        Glide
                .with(getActivity())
                .load(arg.getString(Constants.KEY_IMAGE_URL_ARTIST))
                .apply(new RequestOptions().placeholder(R.drawable.radionovelli))
                .into(imageView);
        songName.setText(arg.getString(Constants.KEY_SONG_NAME));
        if (arg.getString(Constants.KEY_SONG_NAME).length() < 10) {
            songName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        }
        if (arg.getString(Constants.KEY_SONG_NAME).length() > 20) {
            songName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
        }
        if (arg.getString(Constants.KEY_SONG_NAME).length() > 35) {
            songName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 5);
        }
        artistName.setText(arg.getString(Constants.KEY_ARTIST_NAME));
        if (arg.getString(Constants.KEY_ARTIST_NAME).length() < 10) {
            artistName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        }
        if (arg.getString(Constants.KEY_SONG_NAME).length() > 20) {
            artistName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        }
        if (arg.getString(Constants.KEY_ARTIST_NAME).length() > 35) {
            artistName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
        }
        artistName.setTextColor(Color.BLACK);
        return view;
    }


    public void setMusicInfoListener(MusicInfoListener musicInfoListener) {
        this.musicInfoListener = musicInfoListener;
    }


    private void loadApps() {
        manager = getActivity().getPackageManager();
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
        for (ResolveInfo ri : availableActivities) {

            if (ri.loadLabel(manager).equals("YouTube")) {

                youtube.setImageDrawable(ri.activityInfo.loadIcon(manager));
            }


        }
    }


}

