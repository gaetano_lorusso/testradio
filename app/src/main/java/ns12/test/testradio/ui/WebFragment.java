package ns12.test.testradio.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import ns12.test.testradio.R;
import ns12.test.testradio.util.Constants;



public class WebFragment extends android.support.v4.app.Fragment {
    private String url;


    public static WebFragment getInstance(String url){
        Bundle args = new Bundle();
        args.putString(Constants.KEY_URL_WEB_FRAGMENT,url);
        WebFragment webFragment = new WebFragment();
        webFragment.setArguments(args);
        return  webFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        url=bundle.getString(Constants.KEY_URL_WEB_FRAGMENT);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup vg,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.web_fragment, vg, false);
        WebView myWebView = view.findViewById(R.id.webview);

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl(url);

        return view;
    }






}
