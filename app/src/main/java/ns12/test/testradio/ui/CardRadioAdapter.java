package ns12.test.testradio.ui;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import ns12.test.testradio.model.Radio;
import ns12.test.testradio.R;


public class CardRadioAdapter extends RecyclerView.Adapter<CardRadioAdapter.RadioViewHolder>{


    private MainActivity activity;
    private FragmentList fragmentList;

    private List<Radio> radio;


    public List<Radio> getRadio() {
        return radio;
    }

    CardRadioAdapter(List<Radio> ra, Activity c, FragmentList fragmentList){
        activity=(MainActivity) c;
        radio=ra;
        this.fragmentList=fragmentList;


    }

    public int getItemCount() {
        return radio.size();
    }

    /*@Override
    public void onBindViewHolder(PersonViewHolder holder, int position, List<Object> payloads) {
        if(!payloads.isEmpty()) {
            holder.ge
            if (payloads.get(0) instanceof Integer) {
                holder.play.setText(String.valueOf((Integer)payloads.get(0)));
            }
        }else {
            onBindViewHolder(holder, position);
        }
    }*/

    public void onBindViewHolder(final RadioViewHolder radioViewHolder, final int i) {

            Glide.with(activity.getApplicationContext()).load(radio.get(i).getImmagine()).into(radioViewHolder.radioimage);



        radioViewHolder.card.setCardBackgroundColor(activity.getResources().getColor(R.color.card));


        radioViewHolder.card.setOnClickListener(new View.OnClickListener() {//CLICK su item
            @Override
            public void onClick(View v) {
                if(fragmentList.getVisibleItem()==i ){
                            activity.playRadio(radio.get(i));

                }
            }
        });



    }

    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

    }



    public RadioViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card, viewGroup, false);
        RadioViewHolder pvh = new CardRadioAdapter.RadioViewHolder(v);
        return pvh;
    }

    public Radio getItem(int position) {
        return radio.get(position);
    }


    public  class RadioViewHolder extends RecyclerView.ViewHolder {

        public CardView card;
        public ImageView radioimage;


        RadioViewHolder(View itemView) {

            super(itemView);
            card = itemView.findViewById(R.id.cardView);
            radioimage = itemView.findViewById(R.id.radio_image);


        }
    }





}