package ns12.test.testradio.ui

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.speech.tts.TextToSpeech
import android.support.design.widget.NavigationView
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.ProgressBar
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import ns12.test.testradio.R
import ns12.test.testradio.mediaplayer.ServiceAudioStreaming
import ns12.test.testradio.model.MusicInfo
import ns12.test.testradio.model.Radio
import ns12.test.testradio.music_recognition.MusicRecognitor
import ns12.test.testradio.ui.ui_interfaces.CommercialButtonListner
import ns12.test.testradio.ui.ui_interfaces.ControlListener
import ns12.test.testradio.ui.ui_interfaces.MusicInfoListener
import ns12.test.testradio.ui.ui_interfaces.SourceListener
import ns12.test.testradio.ui.viewmodel.MainContract
import ns12.test.testradio.ui.viewmodel.MainViewModel
import ns12.test.testradio.ui.viewmodel.PlayerState
import ns12.test.testradio.ui.viewmodel.SourceType
import ns12.test.testradio.util.Constants
import ns12.test.testradio.util.Constants.RC_SIGN_IN
import ns12.test.testradio.util.Constants.RTL2832U_RESULT_CODE
import ns12.test.testradio.util.NetworkUtil
import ns12.test.testradio.youtube_player.YoutubeListFragment
import ns12.test.testradio.youtube_player.YoutubeListener
import ns12.test.testradio.youtube_player.YoutubePlayerActivity
import ns12.test.testradio.youtube_player.YoutubeVideoModel
import java.util.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, CommercialButtonListner, MainContract.View, SourceListener, ControlListener,MusicInfoListener ,YoutubeListener {


    override fun openYoutube() {
        val fm = supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        val fragmentYoutube = YoutubeListFragment.getInstance(artistName)
        fragmentYoutube.youtubeListener=this
        fragmentTransaction.add(R.id.list_fragment, fragmentYoutube,Constants.FRAGMENT_YOUTUBE)
                .addToBackStack(null).commit()
        searchOnYoutube(artistName)
    }

    override fun openPlayer(video : String) {
        val intent=Intent(this@MainActivity,YoutubePlayerActivity::class.java)
        intent.putExtra("video_id",video)
        startActivity(intent)
    }


    override fun changeSourceListener(sourceType: SourceType) {
        viewModel.selectSource(sourceType)
        when (sourceType){
            SourceType.FM ->playRadio(viewModel.currentRadio!!)
            SourceType.WEB -> prepareForWebRadio()
        }


    }




    fun changeCity(citta : String){
        viewModel.changeCity(citta)
    }




    override fun showWebView(url : String) {
        val webFragment = WebFragment.getInstance(url)
        val fm = supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        fragmentTransaction.add(R.id.list_fragment, webFragment )
                .addToBackStack(null).commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if(id== R.id.logout){
            logout()
        }else if(id==R.id.login){
            startSignInFlow()
        }
        drawer?.closeDrawer(GravityCompat.START)

        return true
    }

    lateinit var viewModel: MainViewModel
    lateinit var artistName : String
    var heightForDialog: Int ? = null
    private var progressDialog: ProgressBar? = null
    var toolbar: Toolbar? = null;
    lateinit var navigationView: NavigationView
    lateinit var recognizer : SpeechRecognizer
    var videoFragment: VideoFragment? = null
    var drawer: DrawerLayout? = null
    var commercialButton : CommercialButton? = null
    var musicInfoDialogFragment : MusicInfoDialogFragment? = null
    lateinit var mMyBroadcastReceiver : NetworkChangeReceiver
    private var mMediaBrowser: MediaBrowserCompat? = null
    private var mMediaController: MediaControllerCompat? = null
    lateinit var textToSpeech : TextToSpeech
    lateinit var musicRecognitor: MusicRecognitor


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        musicRecognitor = MusicRecognitor(this)
        artistName=""

        progressDialog = findViewById(R.id.progressbar)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val fm = supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        val drawerLayout :DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.setBackgroundColor(getResources().getColor(R.color.activity_color));
        if(supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_LIST) ==null){
            var fragmentList =FragmentList()
            
            fragmentList?.setSourceListener(this)
            fragmentTransaction.add(R.id.list_fragment, fragmentList,Constants.FRAGMENT_LIST)
        }else{
            val fragmentList = supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_LIST) as FragmentList
            fragmentList?.setSourceListener(this)
        }
        if(supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) ==null){
            val fragmentControl =FragmentControl.getInstance()
            fragmentControl!!.setControlListener(this)
            fragmentTransaction.add(R.id.control_fragment, fragmentControl,Constants.FRAGMENT_CONTROL)

        }else{
            val fragmentControl = supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) as FragmentControl
            fragmentControl?.setControlListener(this)
        }
         
       
        
        fragmentTransaction.commit()
        supportFragmentManager.executePendingTransactions()
        

       /* drawer = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer?.addDrawerListener(toggle)
        toggle.syncState()

        //How to change elements in the header programatically
        val headerView = navigationView?.getHeaderView(0)
        navigationView?.setNavigationItemSelectedListener(this)*/

        //Nasconde il nome dell'app nel tollbar
        supportActionBar!!.setDisplayShowTitleEnabled(false)
         mMyBroadcastReceiver =NetworkChangeReceiver()

        var filter = IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

        this.registerReceiver(mMyBroadcastReceiver, filter);



        mMediaBrowser = MediaBrowserCompat(this,
                ComponentName(this, ServiceAudioStreaming::class.java),
                mConnectionCallbacks,
                null) // optional Bundle

        viewModel = ViewModelProviders.of(this).get(MainViewModel ::class.java)


        viewModel.frequency.observe(this, object:Observer<Long> {

            override fun onChanged(t: Long?) {
                val fragmentControl=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) as FragmentControl
                fragmentControl?.setFrequency(t.toString())
            }
        })
        musicRecognitor.artist.observe(this, object:Observer<String> {

            override fun onChanged(t: String?) {
                if(t!= null){
                    artistName = t
                }
            }
        })


        viewModel.currentSource.observe(this,object:Observer<SourceType>{
            override fun onChanged(t: SourceType?) {
                changeColor(t!!)
                val fragmentList=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_LIST) as FragmentList
                val fragmentControl=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) as FragmentControl
                when(t){
                    SourceType.WEB -> {
                        fragmentControl?.setInvisbleFrequency()
                        fragmentControl?.setPauseVisibility(true)
                        prepareForWebRadio()

                    }

                    SourceType.FM -> {
                        prepareForFm()
                        fragmentControl?.setPauseVisibility(false)
                    }

                }
            }
        } )

        viewModel.currentPlayerStatus.observe(this,object :Observer<PlayerState>{
            override fun onChanged(t: PlayerState?) {
                when(t){
                    PlayerState.PLAY -> playRadio(viewModel.currentRadio!!)

                }
            }
        })


        /**
         * NOT USED
         */
       /* viewModel.authLive.observe(this,object :Observer<Boolean>{
            override fun onChanged(t: Boolean?) {
                if(t!! == false){
                    createLogin()
                }else{
                    saveAccount(FirebaseAuth.getInstance().currentUser!!.email!!)
                }
            }
        })
*/
        viewModel.checkIsAuth()


        musicRecognitor.showInfoDialog.observe(this,object:Observer<MusicInfo>{
            override fun onChanged(t: MusicInfo?) {
                showInfoDialog(t!!.title,t.artist,t.imageUrl)

            }
        })
        musicRecognitor.showCommercial.observe(this,object:Observer<ArrayList<String>>{
            override fun onChanged(array: ArrayList<String>?) {
                showCommercialButton(array!!)
            }
        })


        viewModel!!.videoList.observe(this, object : Observer<ArrayList<YoutubeVideoModel>> {

            override fun onChanged(t: ArrayList<YoutubeVideoModel>?) {
                if (t!!.size > 0) {
                    val youtubeSetting = getSharedPreferences(Constants.KEY_YOUTUBE_SETTINGS, 0)

                    var youtubePreference = youtubeSetting.getString(getString(R.string.key_youtube),Constants.VALUE_SETTINGS_YOUTUBE)
                    if(youtubePreference.equals(Constants.VALUE_SETTINGS_YOUTUBE)){
                        val fragmentYoutube=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_YOUTUBE) as YoutubeListFragment
                        fragmentYoutube.setUpRecyclerView()
                        fragmentYoutube.populateRecyclerView(t)
                    }else{
                        var intent=Intent(this@MainActivity, YoutubePlayerActivity::class.java)

                        intent.putExtra("lista",t)
                        startActivity(intent)
                        supportFragmentManager.beginTransaction().remove(supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_YOUTUBE)).commit()
                    }
                }
            }
        })
        viewModel!!.isProgress.observe(this, object : Observer<String> {
            override fun onChanged(t: String?) {
                if (t!!.equals("start")) {
                    startProgress()
                } else if (t!!.equals("success")) {
                    stopProgress()

                } else if (t!!.equals("error")) {
                    stopProgress()

                    Toast.makeText(this@MainActivity, " errore", Toast.LENGTH_LONG)
                }
            }
        })
        lifecycle.addObserver(musicRecognitor)


        recognizer = SpeechRecognizer.createSpeechRecognizer(this)
        recognizer.setRecognitionListener(object :RecognitionListener{
            override fun onReadyForSpeech(params: Bundle?) {

            }

            override fun onRmsChanged(rmsdB: Float) {
            }

            override fun onBufferReceived(buffer: ByteArray?) {
            }

            override fun onPartialResults(partialResults: Bundle?) {
               // val data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
            }

            override fun onEvent(eventType: Int, params: Bundle?) {
            }

            override fun onBeginningOfSpeech() {
            }

            override fun onEndOfSpeech() {
            }

            override fun onError(error: Int) {
                Log.e("ERROr","RECOGNIOTION")
                toast("ERRORE ${error}" ,Toast.LENGTH_LONG)
            }

            override fun onResults(results: Bundle) {
                val data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                val scores = results.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES)
                var max = 0
                if(scores != null){
                    for (i in 1.. scores.size-1){
                        if(scores[i] > scores[max]){
                            max = i
                        }
                    }
                }
                val resultString = data[max]

                val fragmentList = supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_LIST) as FragmentList
                if(!fragmentList.selectVocalRadio(resultString)){
                    showConfirmDialog(resultString)
                }
                else{
                    when(viewModel.currentSourceType){
                        SourceType.FM -> {
                            prepareForFm()
                            playRadio(viewModel.currentRadio!!)
                        }
                        SourceType.WEB -> prepareForWebRadio()
                    }
                }

            }
        })

    }


    fun showConfirmDialog(artist: String?) {
        var alertDialog = AlertDialog.Builder(this).create();

        alertDialog.setTitle("Ricerca Artista");

        alertDialog.setMessage("Hai pronunciato : "+artist);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Cerca Video", {
            dialogInterface, i ->
           openYoutube(artist!!)
            
        })

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Annulla", {
            dialogInterface, i ->
            initPlayer()
            alertDialog.dismiss()
        })

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Riprova", {
            dialogInterface, i ->
            alertDialog.dismiss()
            startVoiceRecognition()
        })
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show();
    }

    override fun openYoutube( artist : String) {
        val fm = supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        val fragmentYoutube = YoutubeListFragment.getInstance(artist)
        fragmentYoutube.youtubeListener=this
        fragmentTransaction.add(R.id.list_fragment, fragmentYoutube,Constants.FRAGMENT_YOUTUBE)
                .addToBackStack(null).commit()
        searchOnYoutube(artist)
    }


    inner class MediaBrowserSubscriptionCallback : MediaBrowserCompat.SubscriptionCallback() {

        override fun onChildrenLoaded(parentId: String,
                                      children: List<MediaBrowserCompat.MediaItem>) {


            // Call "playFromMedia" so the UI is updated.
            // mMediaController!!.getTransportControls().prepare()
        }


    }

    fun searchOnYoutube(artist: String?){
        viewModel!!.searchArtistOnYoutube(artist!!)
    }



    var controllerCallback: MediaControllerCompat.Callback = object : MediaControllerCompat.Callback() {
        override fun onMetadataChanged(metadata: MediaMetadataCompat?) {


        }

        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {


        }


        override fun onQueueChanged(queue: MutableList<MediaSessionCompat.QueueItem>?) {
            super.onQueueChanged(queue)
            Log.d("QUEUE", "YES");
        }

        override fun onSessionEvent(event: String?, extras: Bundle?) {
            if (event.equals(Constants.Play)) {
                MediaControllerCompat.getMediaController(this@MainActivity).transportControls.play()
            } else if (event.equals(Constants.PlayFirst)) {
                MediaControllerCompat.getMediaController(this@MainActivity).transportControls.playFromUri(Uri.parse(viewModel.currentRadio!!.url),null)
                //startProgress();
            } else if (event.equals(Constants.START_PROGRESS)) {
                startProgress();
            } else if (event.equals(Constants.STOP_PROGRESS)) {
                stopProgress();
            }else if(event.equals(Constants.ERROR_PLAYER)){
                stopProgress()
                toast("ERRORE NEL CARICARE LA RADIO\n Scegliere un altra radio",Toast.LENGTH_LONG)
            }
        }



    }


    private val mConnectionCallbacks = object : MediaBrowserCompat.ConnectionCallback() {
        override fun onConnected() {

            // Get the token for the MediaSession
            val token = mMediaBrowser!!.getSessionToken()


            // Create a MediaControllerCompat
            mMediaController = null

            mMediaController = MediaControllerCompat(this@MainActivity, // Context
                    token)

            // Save the controller
            MediaControllerCompat.setMediaController(this@MainActivity, mMediaController)

            // Finish building the UI
            buildTransportControls()

            //mMediaBrowser!!.subscribe(mMediaBrowser!!.getRoot(), mMediaBrowserSubscriptionCallback)

            viewModel.onMediaBrowserConnected()
            isConnecting=false
            stopProgress()
        }

        override fun onConnectionSuspended() {
            // The Service has crashed. Disable transport controls until it automatically reconnects
            Log.e("CONENSSIONE ","FALLITA")
        }

        override fun onConnectionFailed() {
            // The Service has refused our connection
            Log.e("FALLITA ","CONNESSIONE")
        }

    }

    private fun buildTransportControls() {
        val mediaController = MediaControllerCompat.getMediaController(this)

        // Register a Callback to stay in sync
        mediaController.registerCallback(controllerCallback)
    }


    private var loading: Boolean = false

    private var selectedRadio: Radio ? = null

    override fun playRadio(radio: Radio?) {
        if (!loading && radio != null ) {
            if(!radio!!.nome.equals("Artist Search")){


                when (viewModel.getSourceType()) {
                    SourceType.WEB -> {
                        val descriptionCompat = MediaDescriptionCompat.Builder().setMediaId("test").setMediaUri(Uri.parse(radio.url)).build()
                        //mMediaController!!.addQueueItem(descriptionCompat, 0)
                        mMediaController!!.transportControls.playFromUri(Uri.parse(radio.url),null)
                        viewModel.play()
                    }

                    SourceType.FM -> {
                        viewModel.play()
                        val fragmentControl=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) as FragmentControl
                        fragmentControl!!.setRadioName(radio.nome)
                    }

                }
            }else{
                startVoiceRecognition()
            }

            selectedRadio= radio

            musicRecognitor.nowOnAir = selectedRadio?.url
        }

    }

    fun startProgress() {

        //progressDialog?.setVisibility(View.VISIBLE);
        if(videoFragment == null){
            videoFragment = VideoFragment()
        }
        videoFragment!!.show(supportFragmentManager,"")
        loading=true
        setClickable(false)

    }

    // DISCONNECT THE MEDIA SESSION AND ITS CALLBACKS
    override fun prepareForFm() {
        if (MediaControllerCompat.getMediaController(this@MainActivity)!=null){
            MediaControllerCompat.getMediaController(this@MainActivity).transportControls.stop()
            //mMediaController!!.unregisterCallback(controllerCallback)
            //mMediaBrowser!!.disconnect()
        }
        val fragmentList=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_LIST) as FragmentList
        val fragmentControl=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) as FragmentControl

        fragmentList!!.setVisibleControlForFrequency(true)
        fragmentControl?.setPauseVisibility(false)
        openRtlDriver()

    }

    private fun openRtlDriver() {

            // We might need to start the driver:

            // start local rtl_tcp instance:
            try {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setClassName("marto.rtl_tcp_andro", "com.sdrtouch.rtlsdr.DeviceOpenActivity")
                intent.data = Uri.parse("iqsrc://-a 127.0.0.1 -p 1234 -n 1")
                startActivityForResult(intent, RTL2832U_RESULT_CODE)
            } catch (e: ActivityNotFoundException) {
                Log.e("RADIOFM", "createSource: RTL2832U is not installed")
            }
    }

    private var isConnecting: Boolean =false

    override fun prepareForWebRadio(){
        shutdownDriver()
        selectedRadio = null
        if (!isConnecting){
            if(!mMediaBrowser!!.isConnected){
                startProgress()
                isConnecting=true
                mMediaBrowser!!.connect()

            }
        }
        playRadio(viewModel.currentRadio)

    }

    override  fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if(viewModel.currentSourceType.equals(SourceType.WEB)){
            prepareForWebRadio()
        }
    }

    override fun updateFrequency(stato : Boolean){
        if(viewModel.currentSourceType.equals(SourceType.FM)) {
            viewModel.updateFrequency(stato, selectedRadio!!)
        }
    }


    override fun manageMediaSession() {

        val pbState = MediaControllerCompat.getMediaController(this@MainActivity).playbackState.state
        if (pbState == PlaybackStateCompat.STATE_PLAYING) {
            MediaControllerCompat.getMediaController(this@MainActivity).transportControls.pause()
        } else {
            MediaControllerCompat.getMediaController(this@MainActivity).transportControls.play()
        }
    }

    fun stopProgress() {
        val fragmentControl=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) as FragmentControl?
        fragmentControl?.setRadioName(selectedRadio?.nome)
        progressDialog?.setVisibility(View.INVISIBLE);
        loading=false

        videoFragment?.dismiss()

        setClickable(true)
    }


    override fun showInfoDialog(song: String, artist: String?, imageUrl: String?) {
        runOnUiThread {

            if (musicInfoDialogFragment != null) {
                musicInfoDialogFragment!!.dismiss()
                musicInfoDialogFragment = null
                showInfoDialog(song, artist, imageUrl)
            }
            else{
                val fm = supportFragmentManager
                musicInfoDialogFragment = MusicInfoDialogFragment.getInstance( song, artist, imageUrl,toolbar!!.height)
                musicInfoDialogFragment!!.setMusicInfoListener(this)
               musicInfoDialogFragment!!.show(fm,"Music Info")
            }
        }
    }

     fun showCommercialButton(array: ArrayList<String>){
         val fm = supportFragmentManager
         val fragment=fm.findFragmentByTag(Constants.FRAGMENT_COMMERCIAL)

        if(fragment == null){

            commercialButton = CommercialButton.getInstance(array[0],array[1], currentRadio?.codice, toolbar!!.height,this)
            commercialButton!!.show(fm,Constants.FRAGMENT_COMMERCIAL)
        }else{
            commercialButton=fragment as CommercialButton
            commercialButton!!.setInfo(array[0],array[1])
        }
    }


    override fun  onCreateOptionsMenu( menu : Menu)  : Boolean
    {
        val  inflater : MenuInflater=getMenuInflater();
        inflater.inflate(R.menu.menu_settings,menu);
        return true;
    }


    override  fun onOptionsItemSelected(item : MenuItem ): Boolean{
        val id = item.getItemId();

        if (id == R.id.impostazioni) {
            // launch settings activity
            val myPreferenceFragment = MyPreferenceFragment()
            val fm = fragmentManager
            val fragmentTransaction = fm.beginTransaction()

            fragmentTransaction.add(R.id.list_fragment, myPreferenceFragment )
                    .addToBackStack(null).commit()

            return true
        }


        return super.onOptionsItemSelected(item);
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)




        // err_info from RTL2832U:
        val rtlsdrErrInfo = arrayOf("permission_denied", "root_required", "no_devices_found", "unknown_error", "replug", "already_running")

        when (requestCode) {
            Constants.RTL2832U_RESULT_CODE -> {
                // This happens if the RTL2832U driver was started.
                // We check for errors and print them:
                if (resultCode == Activity.RESULT_OK)
                    Log.i("RADIO FM", "onActivityResult: RTL2832U driver was successfully started.")
                else {
                    var errorId = -1
                    var exceptionCode = 0
                    var detailedDescription: String? = null
                    if (data != null) {
                        errorId = data.getIntExtra("marto.rtl_tcp_andro.RtlTcpExceptionId", -1)
                        exceptionCode = data.getIntExtra("detailed_exception_code", 0)
                        detailedDescription = data.getStringExtra("detailed_exception_message")
                    }
                    var errorMsg = "ERROR NOT SPECIFIED"
                    if (errorId >= 0 && errorId < rtlsdrErrInfo.size)
                        errorMsg = rtlsdrErrInfo[errorId]

                    Log.e("RADIO FM", "onActivityResult: RTL2832U driver returned with error: " + errorMsg + " (" + errorId + ")"
                            + if (detailedDescription != null) ": $detailedDescription ($exceptionCode)" else "")


                    Toast.makeText(this@MainActivity, "Error with Source : " + errorMsg + " (" + errorId + ")"
                            + if (detailedDescription != null) ": $detailedDescription ($exceptionCode)" else "", Toast.LENGTH_LONG).show()


                    viewModel.manageDriverError()
                }
            }
            Constants.RC_SIGN_IN -> {

                val response = IdpResponse.fromResultIntent(data)
                Log.e("response",""+response.toString())
                // Successfully signed in
                if (resultCode == Activity.RESULT_OK) {

                    viewModel.checkIsAuth()
                    //saveAccount(data!!.getStringExtra(""))
                    return
                } else {
                    // Sign in failed
                    if (response == null) {
                        // User pressed back button
                        toast("CANCEL",Toast.LENGTH_LONG)
                        return
                    }

                    if (response.error!!.errorCode == ErrorCodes.NO_NETWORK) {
                        toast("NO_NETWORK",Toast.LENGTH_LONG)
                        return
                    }

                    if (response.error!!.errorCode == ErrorCodes.UNKNOWN_ERROR) {
                        toast("ERROE",Toast.LENGTH_LONG)
                        return
                    }

                }

                toast("UNKNOWN",Toast.LENGTH_LONG)
            }
        }

    }



    override fun onDestroy() {
        Log.e("ON ","DESTROY")
        super.onDestroy()
        if(mMediaController!=null){
            if(mMediaController!!.playbackState.state==PlaybackStateCompat.STATE_PLAYING){
                mMediaController!!.transportControls.stop()
            }
            mMediaBrowser!!.disconnect()
        }

        shutdownDriver()

        unregisterReceiver(mMyBroadcastReceiver)

        recognizer.destroy()
    }

    fun shutdownDriver(){
        //SHUTDOWN RTL DRIVER
        try {
            viewModel.stopAnalyzer()
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setClassName("marto.rtl_tcp_andro", "com.sdrtouch.rtlsdr.DeviceOpenActivity")
            intent.data = Uri.parse("iqsrc://-x")    // -x is invalid. will cause the driver to shut down (if runningRadioFm)
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Log.e("FM RADIO", "onDestroy: RTL2832U is not installed")
        }

    }


    override fun changeImage(isPlaying: Boolean) {
    }

    override fun onStart() {
        super.onStart()
        Log.e("on start","")
        heightForDialog=toolbar!!.height
    }

    override fun onResume() {
        super.onResume()
        Log.e("ON","RESUME")
    }


    override fun onRadioSelected(radio: Radio) {
        viewModel.onRadioSelected(radio)
    }

    override fun play() {
        viewModel.play()
        when(viewModel.currentSourceType){
            SourceType.WEB -> manageMediaSession()
        }
    }

    override fun getCurrentRadio() : Radio?{
        return viewModel.currentRadio
    }

    override fun pause() {
        viewModel.pause()
        when(viewModel.currentSourceType){
            SourceType.WEB -> manageMediaSession()
        }

    }


    fun setClickable(clickable: Boolean) {
        val fragmentList=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_LIST) as FragmentList?
        val fragmentControl=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) as FragmentControl?
        fragmentList?.setClickable(clickable)
        fragmentControl?.setClickable(clickable)
    }

    override fun getSelectedRadio() : Radio?{
        return selectedRadio
    }

    override fun toast(message: CharSequence, duration: Int) {
        Toast.makeText(this, message, duration).show()
    }

    override fun changeColor(sourceType: SourceType) {
        val fragmentList=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_LIST) as FragmentList
        fragmentList?.setSourceSelected(sourceType)
    }

    override fun onPause() {
        Log.e("ON ","PAUSE")
        super.onPause()
    }

    override fun onStop() {
        Log.e("ON ","STOP")
        super.onStop()
    }

    inner  class NetworkChangeReceiver: BroadcastReceiver() {


        override  fun onReceive(context: Context, intent: Intent) {

            var status = NetworkUtil.getConnectivityStatusString(context);
            val fragmentList=supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_LIST) as FragmentList
            val fragmentControl =supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_CONTROL) as FragmentControl
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                if (status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                    if (viewModel.getSourceType()==SourceType.WEB){
                        if (progressDialog?.visibility==View.VISIBLE){
                            stopProgress()
                            fragmentControl?.setPauseVisibility(true)
                        }

                        viewModel.selectSource(SourceType.FM)

                        fragmentList?.setSourceSelected(SourceType.FM)
                        fragmentList?.setVisibleControlForFrequency(true)
                        fragmentControl?.setPauseVisibility(false)
                        toast("Disconnesso da internet",Toast.LENGTH_LONG)
                    }
                }

            }
        }
    }

    override fun closeMusicPlayer(){
        when (viewModel.getSourceType()) {
            SourceType.WEB -> {
                if (MediaControllerCompat.getMediaController(this@MainActivity)!=null){
                    MediaControllerCompat.getMediaController(this@MainActivity).transportControls.stop()
                    mMediaController!!.unregisterCallback(controllerCallback)
                    mMediaBrowser!!.disconnect()
                }
            }
            SourceType.FM->{
                shutdownDriver()
            }
        }
    }


    fun startSignInFlow(){
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(Arrays.asList(
                                AuthUI.IdpConfig.EmailBuilder().build(),
                                AuthUI.IdpConfig.GoogleBuilder().build(),
                                AuthUI.IdpConfig.FacebookBuilder().build()
                        ))
                        .build(),
                RC_SIGN_IN)
    }

    /**
     * NOT USED
     */
    /*fun createLogin(){
        navigationView!!.getMenu().clear();
        navigationView!!.inflateMenu(R.menu.menu_login);
        label.setText("Non sei Autenticato")
        this.account.setText("")
    }*/

    /**
     * NOT USED
     */
  /*  fun saveAccount(account : String ){
        navigationView!!.getMenu().clear();
        navigationView!!.inflateMenu(R.menu.menu_logout);
        label.setText("Registrato come :")
        this.account.setText(account)
    }
*/
    fun logout(){
        startProgress()
        AuthUI.getInstance()
                .signOut(this)
        stopProgress()
        viewModel.checkIsAuth()
    }

    override fun startVoiceRecognition(){
        closeMusicPlayer()
        startRecognition()
    }


    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        when(keyCode){
            KeyEvent.KEYCODE_F1 -> {
                updateFrequency(true)
                return true
            }
            KeyEvent.KEYCODE_F2 -> {
                updateFrequency(false)
                return true
            }
            else ->return super.onKeyUp(keyCode, event)
        }
    }

    fun startRecognition(){
        runOnUiThread {
            if (SpeechRecognizer.isRecognitionAvailable(this)) {
                val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
                intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "voice.recognition.test");
                recognizer.startListening(intent);
            }
            else {
                toast("RECOGNIZER NOT AVAILABLE", Toast.LENGTH_LONG)
            }
        }
    }

    fun initPlayer(){
        val sourceType=viewModel.currentSourceType
        viewModel.currentSourceType=SourceType.NULL
        viewModel.selectSource(sourceType)
    }

}

