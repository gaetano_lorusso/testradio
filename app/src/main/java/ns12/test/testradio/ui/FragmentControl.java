package ns12.test.testradio.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import ns12.test.testradio.R;
import ns12.test.testradio.ui.ui_interfaces.ControlListener;



public class FragmentControl extends android.support.v4.app.Fragment {
    private View view;
    private TextView textView;
    private ImageView play;
    private ImageView pause;
    private Context context;
    private ControlListener activity;
    private TextView label;
    private String radioName;
    private boolean b=false;
    private ControlListener controlListener;
    private TextView frequency;
    private TextView artistName;
    private ImageView microphone;

    public static FragmentControl getInstance(){

        FragmentControl fragmentControl = new FragmentControl();

        return  fragmentControl;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup vg,Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_control, vg, false);
        textView=view.findViewById(R.id.nome_Radio);
        label=view.findViewById(R.id.label_radio);
        play=view.findViewById(R.id.play);
        pause=view.findViewById(R.id.pause);
        frequency=view.findViewById(R.id.frequency);
        artistName = view.findViewById(R.id.nomeArtista);

        textView.setTextColor(Color.BLACK);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play.startAnimation(AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.click));
                if(controlListener.getSelectedRadio()==null) {
                    controlListener.playRadio(controlListener.getCurrentRadio());
                }else {
                    if(controlListener.getSelectedRadio()!=controlListener.getCurrentRadio()){
                        controlListener.playRadio(controlListener.getCurrentRadio());
                    }else {
                        controlListener.play();
                    }
                }


                /*MusicInfoDialogFragment m2=MusicInfoDialogFragment.getInstance("canzone corta","prova","https://encrypted-tbn0.gstatic.com/plays?q=tbn:ANd9GcSsq-nxT7kLCV-pxTlDjoU-VduqNK2UsiOu4r8vhYyAjTEf-HXX",96);
                FragmentManager fm = getFragmentManager();
                m2.show(fm ,"Music Info");*/


                /*MusicInfoDialog m = new MusicInfoDialog(getActivity(), "prova", "prova", "https://encrypted-tbn0.gstatic.com/plays?q=tbn:ANd9GcSsq-nxT7kLCV-pxTlDjoU-VduqNK2UsiOu4r8vhYyAjTEf-HXX");
                CommercialButton b=new CommercialButton(getActivity());
                m.show();*/
                /*FragmentManager fm = getFragmentManager();
                CommercialButton commercialButton=CommercialButton.getInstance("PROVAPRODTOO",96);
                commercialButton.show(fm ,"COmmercial BUTTON");*/

            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pause.startAnimation(AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.click));
                controlListener.pause();
            }
        });
        microphone=view.findViewById(R.id.microphone);
        microphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                microphone.startAnimation(AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.click));
                controlListener.startVoiceRecognition();
            }
        });
        ImageView video=view.findViewById(R.id.video);
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controlListener.openYoutube();
            }
        });
        return view;
    }

    public void setRadioName(String radio){
        if (textView!=null){
            label.setText("Radio On Air:");
            textView.setText(radio);
        }
    }



    public void setFrequency(String frequency){
        this.frequency.setVisibility(View.VISIBLE);
        this.frequency.setText("Frequenza : "+frequency);
    }

    public void setInvisbleFrequency(){
        this.frequency.setVisibility(View.INVISIBLE);
    }

    public void setArtist(String name){

    }

    public void setPauseVisibility(boolean visible){
        if (visible) {
            pause.setVisibility(View.VISIBLE);
        }
        else pause.setVisibility(View.GONE);
    }



    public void setControlListener(ControlListener controlListener) {
        this.controlListener = controlListener;
    }

    public void setClickable(boolean clickable){
        play.setClickable(clickable);
        pause.setClickable(clickable);
    }
}
