package ns12.test.testradio.ui.viewmodel

import ns12.test.testradio.model.Radio

/**
 * Created by userns12 on 12/13/17.
 */
interface MainContract {


    interface ViewModel {

        fun loadRadioList()

        fun selectSource(sourceType: SourceType)

        fun play()

        fun pause()

        fun onDestroy()

        fun getSourceType() : SourceType

        fun onRadioSelected(selectedRadio : Radio)

        fun manageDriverError()

        fun onMediaBrowserConnected()


    }


    interface View {


        fun showInfoDialog(trackTitle: String, artist: String?, imageUrl: String?)
        fun manageMediaSession()
        fun prepareForFm()
        fun changeImage(isPlaying : Boolean)
        fun prepareForWebRadio()
        fun toast(message: CharSequence, duration: Int)
        fun changeColor(sourceType: SourceType)

    }


}