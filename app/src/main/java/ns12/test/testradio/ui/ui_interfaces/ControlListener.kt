package ns12.test.testradio.ui.ui_interfaces

import ns12.test.testradio.model.Radio

/**
 * Created by userns12 on 12/21/17.
 */
interface ControlListener {

    fun play()
    fun openYoutube()
    fun pause()
    fun getSelectedRadio(): Radio?
    fun playRadio(radio : Radio?)
    fun getCurrentRadio() : Radio?
    fun startVoiceRecognition()

}