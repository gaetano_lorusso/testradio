package ns12.test.testradio.ui.ui_interfaces


interface MusicInfoListener {

    fun closeMusicPlayer()

    fun openYoutube(artist: String)
}