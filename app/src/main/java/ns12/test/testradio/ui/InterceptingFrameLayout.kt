package ns12.test.testradio.ui

import android.content.Context
import android.support.v4.view.MotionEventCompat
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.FrameLayout

/**
 * Created by userns12 on 1/26/18.
 */
class InterceptingFrameLayout : FrameLayout {

    var listener : FrameTouch? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {

        when(ev!!.action) {
            MotionEvent.ACTION_DOWN ->
                    return true
        }


        return super.onInterceptTouchEvent(ev)
    }




    override fun onTouchEvent(event: MotionEvent?): Boolean {
        (listener)?.onFrameTouched()
        return super.onTouchEvent(event)
    }


    interface FrameTouch {

        fun onFrameTouched()
    }


}