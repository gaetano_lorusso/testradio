package ns12.test.testradio.custom_search;

import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import ns12.test.testradio.util.Constants;

import static com.google.api.client.extensions.android.http.AndroidHttp.newCompatibleTransport;


/**
 * Created by userns12 on 12/4/17.
 */

public class SearchClient {


    private static final int HTTP_REQUEST_TIMEOUT = 20000;


    public static String search(String keyword){
        Customsearch customsearch= null;


        try {
            customsearch = new Customsearch(newCompatibleTransport(),new AndroidJsonFactory(), new HttpRequestInitializer() {
                public void initialize(HttpRequest httpRequest) {
                    try {
                        // set connect and read timeouts
                        httpRequest.setConnectTimeout(HTTP_REQUEST_TIMEOUT);
                        httpRequest.setReadTimeout(HTTP_REQUEST_TIMEOUT);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Result> resultList=null;
        try {
            String toSearch =keyword.replaceAll("\\s+","+")+"+singer";
            Customsearch.Cse.List list=customsearch.cse().list((toSearch));
            list.setKey(Constants.CUSTOM_SEARCH_API_KEY);
            list.setCx(Constants.SEARCH_ENGINE_ID);
            list.setSearchType("image");
            Search results=list.execute();
            resultList=results.getItems();
            for(Result result : resultList){
                String imageUrl = result.getLink();
                if(imageUrl != null && imageUrl.length() >0){
                    return imageUrl;
                }
            }
        }
        catch (  Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
