package ns12.test.testradio.custom_search;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import ns12.test.testradio.util.Constants;

/**
 * Created by userns12 on 12/4/17.
 */

public class SearchService extends IntentService {

    public SearchService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String query = intent.getStringExtra(Constants.KEY_ARTIST_NAME);
        String imageUrl = SearchClient.search(query);

    }


}
