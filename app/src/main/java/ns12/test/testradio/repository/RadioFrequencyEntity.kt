package ns12.test.testradio.Repository

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "radiofrequency",
        foreignKeys = arrayOf(ForeignKey(entity = RadioEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("radioid"),
        onDelete = ForeignKey.CASCADE)))
data class RadioFrequencyEntity(@ColumnInfo(name = "regione") var regione : String="",
                            @ColumnInfo(name="citta") var citta : String="",
                            @ColumnInfo(name="frequency") var frequency : String="",
                            @ColumnInfo(name="radioid") var radioid : Int=0)
{
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true) var id: Int = 0


    companion object Data{
        fun getRadioFrequency() : ArrayList<RadioFrequencyEntity> {
            var list=ArrayList<RadioFrequencyEntity>()
            list.add(RadioFrequencyEntity("lazio" , "Roma","100700000",1))
            list.add(RadioFrequencyEntity("lazio" , "Roma","96100000",2))
            list.add(RadioFrequencyEntity("lazio" , "Roma","98700000",3))
            list.add(RadioFrequencyEntity("lazio" , "Roma","103000000",4))
            list.add(RadioFrequencyEntity("lazio" , "Roma","91500000",5))
            list.add(RadioFrequencyEntity("lazio" , "Roma","104200000",6))
            list.add(RadioFrequencyEntity("lazio" , "Roma","190640000",7))
            list.add(RadioFrequencyEntity("lazio" , "Roma","88900000",8))
            list.add(RadioFrequencyEntity("lazio" , "Roma","97200000",9))
            list.add(RadioFrequencyEntity("lazio" , "Roma","87600000",10))
            list.add(RadioFrequencyEntity("lazio" , "Roma","97000000",11))
            list.add((RadioFrequencyEntity("lazio" , "Roma","106100000",12)))


/*
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","90300000",1))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","96100000",2))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","98700000",3))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","91450000",4))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","1030000000",5))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","104200000",6))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","",7))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","97000000",8))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","88900000",9))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","97200000",10))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","87600000",11))
            list.add(RadioFrequencyEntity("lazio" , "Frosinone","94000000",12))
            list.add((RadioFrequencyEntity("lazio" , "Frosinone","106100000",13)))
            list.add(RadioFrequencyEntity("lazio","Roma","0",14))*/



            return list
        }
    }




}