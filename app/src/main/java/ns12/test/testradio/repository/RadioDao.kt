package ns12.test.testradio.Repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.REPLACE

/**
 * Created by userns12 on 1/4/18.
 */
@Dao
interface RadioDao  {

    @Query("select * from radio")
    fun getAllRadio() : List<RadioEntity>

    @Query("select * from radio where name = :name")
    fun getRadioByName(name : String) : LiveData<RadioEntity>

    @Query("select * from radiofrequency where citta = :citta")
    fun getFrequencyByCity(citta : String) : List<RadioFrequencyEntity>

    /*@Query("select rf.frequency from radiofrequency rf, radio r where r.name = :arg0 and r.id=rf.radioid and rf.citta=:arg1 ")
    fun getRadioFrequencyByCity(name : String, citta : String) : LiveData<RadioEntity>*/

    @Insert(onConflict = REPLACE)
    fun insertFrequency(arrayList: ArrayList<RadioFrequencyEntity>)

    @Insert(onConflict = REPLACE)
    fun insertRadio(arrayList: ArrayList<RadioEntity>)

    @Update(onConflict = REPLACE)
    fun updateRadio(radioEntity: RadioEntity)

    @Delete
    fun deleteTask(radioEntity: RadioEntity)


}