package ns12.test.testradio.Repository

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by userns12 on 1/4/18.
 */
@Entity(tableName = "radio")
data class RadioEntity(
                       @ColumnInfo(name = "name") var name : String ="",
                       @ColumnInfo(name="web") var web : String ="",
                       @ColumnInfo(name="imageurl") var imageurl : String="",
                       @ColumnInfo(name="codice") var codice : String ="",
                       @ColumnInfo(name = "id")
                        @PrimaryKey var id: Int = 0) {


    companion object {
        fun loadRadio(): ArrayList<RadioEntity> {

            var list = ArrayList<RadioEntity>()
            list.add(RadioEntity("Radio Deejay", "https://www.deejay.it/webradio/01/", "http://goo.gl/f5zUJD", id = 1))
            list.add(RadioEntity("Radio 105", "http://bit.ly/2uMqxr3", "http://goo.gl/uQ67oC",id = 2))
            list.add(RadioEntity("Virgin Radio", "http://bit.ly/2wl1cTn", "http://goo.gl/TjR9gk",id = 3))
            list.add(RadioEntity("RTL 102.5", "http://shoutcast.rtl.it:3010", "http://goo.gl/axwtai", id = 4))

            list.add(RadioEntity("Radio Italia", "http://sr9.inmystream.info:8006", "http://goo.gl/6qatB3",id = 5))

            list.add(RadioEntity("Radio Novelli", "http://bit.ly/2uMqrjb", "http://radionovelli.it/img/logo_radionovelli_2016_contorno.png", codice = "RADIO_NOVELLI",id = 6))
            list.add(RadioEntity("RDS", "http://bit.ly/2uVgMXN", "http://goo.gl/NHgbZQ",id = 7))
            list.add(RadioEntity("Radio Zeta L’Italiana", "http://bit.ly/2gGAdvI", "http://goo.gl/ybMnHC", id = 8))
            list.add(RadioEntity("Radio Kiss Kiss", "http://bit.ly/2lmkRBN", "http://goo.gl/MZmhpR", id = 9))
            list.add(RadioEntity("Rai Radio 1", "http://bit.ly/2qeNWfe", "http://goo.gl/pzG71h", id = 10))
            list.add(RadioEntity("Radio Subasio", "http://bit.ly/2yOQ3wt", "http://goo.gl/VgRucX", id = 11))
            list.add((RadioEntity("Radio Monte Carlo", "http://bit.ly/2uVmuJq", "http://goo.gl/Qe9SAF", id = 12)))
            return list
        }
    }
}


