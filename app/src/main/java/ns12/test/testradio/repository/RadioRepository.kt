package ns12.test.testradio.Repository

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import ns12.test.testradio.util.ioThread

/**
 * Created by userns12 on 1/4/18.
 */
@Database(entities = arrayOf(RadioEntity::class,RadioFrequencyEntity::class), version = 2, exportSchema = false)
abstract class RadioDatabase : RoomDatabase() {

    abstract fun radioDao() : RadioDao


    companion object {

        @Volatile private var INSTANCE: RadioDatabase? = null

        fun getInstance(context: Context): RadioDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }


        private fun buildDatabase(context: Context) =Room.databaseBuilder(context.applicationContext,RadioDatabase::class.java, "Sample.db")
                        // prepopulate the database after onCreate was called
                        .addCallback(object : Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                // insert the data on the IO Thread
                                ioThread {
                                    getInstance(context).radioDao().insertRadio(RadioEntity.loadRadio())
                                    getInstance(context).radioDao().insertFrequency(RadioFrequencyEntity.getRadioFrequency())
                                }
                            }
                        })
                        .fallbackToDestructiveMigration()
                        .build()

        }



    }