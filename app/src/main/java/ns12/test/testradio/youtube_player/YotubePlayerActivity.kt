package ns12.test.testradio.youtube_player

import android.content.Intent
import com.google.android.youtube.player.internal.e
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import android.content.Intent.getIntent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.google.android.youtube.player.YouTubePlayerView
import com.google.android.youtube.player.YouTubeBaseActivity
import ns12.test.testradio.R
import ns12.test.testradio.ui.MainActivity
import ns12.test.testradio.util.Constants


class YoutubePlayerActivity : YouTubeBaseActivity() {
    private var videoID: String? = null
    private var youTubePlayerView: YouTubePlayerView? = null
    private var arrayVideo= ArrayList<YoutubeVideoModel>()
    private var list : Boolean=false

    override fun onCreate( savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.youtube_player)
        if(intent.extras.getSerializable("lista")!=null){
            arrayVideo=intent.extras.getSerializable("lista") as (ArrayList<YoutubeVideoModel>)
            list=true
        }else{
            videoID = intent.getStringExtra("video_id")
            list=false
        }

        youTubePlayerView = findViewById(R.id.youtube_player_view)
        initializeYoutubePlayer()
    }

    /**
     * initialize the youtube player
     */
    private fun initializeYoutubePlayer() {
        youTubePlayerView!!.initialize(Constants.KEY_YOUTUBE, object : YouTubePlayer.OnInitializedListener {

            override fun onInitializationSuccess(provider: YouTubePlayer.Provider, youTubePlayer: YouTubePlayer,
                                                 wasRestored: Boolean) {

                //if initialization success then load the video id to youtube player
                if (!wasRestored) {
                    //set the player style here: like CHROMELESS, MINIMAL, DEFAULT
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT)
                    youTubePlayer.setFullscreen(true)
                    //load the video
                    if(videoID!=null){
                        youTubePlayer.loadVideo(videoID)
                        youTubePlayer.play()
                    }else{
                        val array=ArrayList<String>()
                        for(item : YoutubeVideoModel in arrayVideo){
                            array.add(item.videoId)
                        }
                        youTubePlayer.cueVideos(array)
                        var handler =Handler()
                        handler.postDelayed(Runnable { youTubePlayer.play() },1000)
                    }







                }
            }

            override fun onInitializationFailure(arg0: YouTubePlayer.Provider, arg1: YouTubeInitializationResult) {
                //print or show error if initialization failed
                Toast.makeText(baseContext,"Errore nel caricamento",Toast.LENGTH_LONG)
            }
        })
    }

    companion object {
        private val TAG = YoutubePlayerActivity::class.java.simpleName
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if(list){
            var intent= Intent(baseContext, MainActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            startActivity(intent)
            this.finish()
        }

    }

}
