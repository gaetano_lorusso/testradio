package ns12.test.testradio.youtube_player

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import com.google.api.client.http.HttpRequest
import com.google.api.client.http.HttpRequestInitializer
import com.google.api.services.youtube.YouTube
import ns12.test.testradio.R
import ns12.test.testradio.util.ioThread
import java.io.IOException

/**
 * Created by userns12 on 1/11/18.
 */
class YoutubeViewModel(val app : Application) : AndroidViewModel(app), YoutubeSearchResult {

    var videoList : MutableLiveData<ArrayList<YoutubeVideoModel>> = MutableLiveData()
    var isProgress : MutableLiveData<String> = MutableLiveData()


    fun searchArtistOnYoutube(artist : String){
        isProgress.postValue("start")
        val apiKey = app.getString(R.string.youtube_api_key)
        ioThread {
            Search_YouTube.getYouTubeSearch(artist,apiKey,this);
        }

    }



    override fun onSuccess(videoIds: ArrayList<YoutubeVideoModel>) {

        isProgress.postValue("success")
        videoList.postValue(videoIds)
    }

    override fun onError() {
        isProgress.postValue("error")
    }




}


interface YoutubeSearchResult {

    fun onSuccess(videoIds : ArrayList<YoutubeVideoModel>)

    fun onError()
}