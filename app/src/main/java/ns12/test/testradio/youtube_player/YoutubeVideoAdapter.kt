package ns12.test.testradio.youtube_player


import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation

import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubeThumbnailLoader
import com.google.android.youtube.player.YouTubeThumbnailView

import ns12.test.testradio.R
import ns12.test.testradio.util.Constants

import java.util.ArrayList



class YoutubeVideoAdapter(private val context: Context, private val youtubeVideoModelArrayList: ArrayList<YoutubeVideoModel>?,private val artist:String) : RecyclerView.Adapter<YoutubeListFragment.YoutubeViewHolder>() {
    var listener : YoutubeListener ? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YoutubeListFragment.YoutubeViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.card_youtube, parent, false)
        return YoutubeListFragment.YoutubeViewHolder(view)
    }

    override fun onBindViewHolder(holder: YoutubeListFragment.YoutubeViewHolder, position: Int) {

        val youtubeVideoModel = youtubeVideoModelArrayList!![position]
        holder.artist.setText(artist)
        holder.videoTitle.setText(youtubeVideoModel.getTitle())
        holder.card.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
               listener!!.openPlayer(youtubeVideoModel.videoId)}
        })


        /*  initialize the thumbnail image view , we need to pass Developer Key */
        holder.videoThumbnailImageView.initialize(Constants.KEY_YOUTUBE, object : YouTubeThumbnailView.OnInitializedListener {
            override fun onInitializationSuccess(youTubeThumbnailView: YouTubeThumbnailView, youTubeThumbnailLoader: YouTubeThumbnailLoader) {
                //when initialization is sucess, set the video id to thumbnail to load
                youTubeThumbnailLoader.setVideo(youtubeVideoModel.getVideoId())

                youTubeThumbnailLoader.setOnThumbnailLoadedListener(object : YouTubeThumbnailLoader.OnThumbnailLoadedListener {
                    override fun onThumbnailLoaded(youTubeThumbnailView: YouTubeThumbnailView, s: String) {
                        //when thumbnail loaded successfully release the thumbnail loader as we are showing thumbnail in adapter
                        youTubeThumbnailLoader.release()
                    }

                    override fun onThumbnailError(youTubeThumbnailView: YouTubeThumbnailView, errorReason: YouTubeThumbnailLoader.ErrorReason) {
                        //print or show error when thumbnail load failed
                        Log.e(TAG, "Youtube Thumbnail Error")
                    }
                })
            }

            override fun onInitializationFailure(youTubeThumbnailView: YouTubeThumbnailView, youTubeInitializationResult: YouTubeInitializationResult) {
                //print or show error when initialization failed
                Log.e(TAG, "Youtube Initialization Failure")

            }
        })

    }

    override fun getItemCount(): Int {
        return youtubeVideoModelArrayList?.size ?: 0
    }

    companion object {
        private val TAG = YoutubeVideoAdapter::class.java.simpleName
    }


}