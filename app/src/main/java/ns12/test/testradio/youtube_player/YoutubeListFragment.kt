package ns12.test.testradio.youtube_player

import android.app.Fragment
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import ns12.test.testradio.R
import ns12.test.testradio.util.Constants.KEY_ARTIST_NAME
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import ns12.test.testradio.ui.MainActivity
import android.content.Intent
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.youtube.player.YouTubeThumbnailView
import ns12.test.testradio.ui.FragmentList
import ns12.test.testradio.util.Constants


/**
 * Created by userns12 on 1/11/18.
 */
class YoutubeListFragment : android.support.v4.app.Fragment(){


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        layout = inflater?.inflate(R.layout.youtubelist, container, false)



        progressBar = layout?.findViewById(R.id.progress_bar)





        return layout
    }


    lateinit var artistName : String
    lateinit var recyclerView: RecyclerView
    var youtubeListener : YoutubeListener ? = null
    var progressBar : ProgressBar ? = null
    var layout : View ? = null

    companion object {
        fun getInstance(artist: String): YoutubeListFragment {

            val args = Bundle()
            args.putString(Constants.KEY_ARTIST_NAME,artist);
            val fragmentYoutube=YoutubeListFragment()
            fragmentYoutube.setArguments(args)
            return fragmentYoutube
        }
    }

    private var youtubeViewModel: YoutubeViewModel ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        youtubeViewModel = ViewModelProviders.of(this@YoutubeListFragment).get(YoutubeViewModel::class.java)
        artistName = arguments!!.getString(KEY_ARTIST_NAME)
    }


    private var adapter: YoutubeVideoAdapter ? = null

     fun populateRecyclerView(youtubeVideoModelArrayList : ArrayList<YoutubeVideoModel>) {

        adapter = YoutubeVideoAdapter(activity!!.applicationContext, youtubeVideoModelArrayList,artistName)
        recyclerView.adapter = adapter
        adapter!!.listener=youtubeListener


    }


    fun setUpRecyclerView() {
        recyclerView = layout?.findViewById<View>(R.id.recycler_view) as RecyclerView

        val linearLayoutManager = LinearLayoutManager(activity)
        recyclerView.setLayoutManager(linearLayoutManager)
    }

    fun launchProgress(){
        progressBar!!.setVisibility(View.VISIBLE)
    }

    fun stopProgress(){
        progressBar!!.setVisibility(View.INVISIBLE)
    }

    class YoutubeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var videoThumbnailImageView: YouTubeThumbnailView
        var videoTitle: TextView
        var card: CardView
        var artist: TextView

        init {
            artist=itemView.findViewById(R.id.artist)
            card=itemView.findViewById(R.id.card_youtube)

            videoThumbnailImageView = itemView.findViewById(R.id.video_thumbnail_image_view)
            videoTitle = itemView.findViewById(R.id.video_title_label)

        }
    }


}