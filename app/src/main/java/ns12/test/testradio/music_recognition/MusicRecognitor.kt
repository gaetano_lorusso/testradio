package ns12.test.testradio.music_recognition

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import com.gracenote.mmid.MobileSDK.GNConfig
import com.gracenote.mmid.MobileSDK.GNOperations
import com.gracenote.mmid.MobileSDK.GNSearchResult
import com.gracenote.mmid.MobileSDK.GNSearchResultReady
import ns12.test.testradio.custom_search.SearchClient
import ns12.test.testradio.model.MusicInfo
import ns12.test.testradio.model.SingleLiveEvent
import wseemann.media.FFmpegMediaMetadataRetriever
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

/**
 * Created by userns12 on 1/24/18.
 */
class MusicRecognitor(val context : Context) : LifecycleObserver {

    val config = GNConfig.init("527200373-50DB49AFC399181D73D3F13F45C8924F", context.applicationContext)
    var trackTitle : String? = null
    val showInfoDialog : SingleLiveEvent<MusicInfo> = SingleLiveEvent()
    val showCommercial: MutableLiveData<ArrayList<String>> = MutableLiveData()
    val artist : MutableLiveData<String> = MutableLiveData()
     var artistName : String ? = null
    val scheduleTaskExecutor  = Executors.newScheduledThreadPool(2);
    var futureMusic : ScheduledFuture<*>? = null
    var futureCommercial: ScheduledFuture<*>? = null
    var nowOnAir : String? = null
    var commercialName: String? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun recognizeMusic() {
        if(futureMusic == null || futureMusic!!.isCancelled){
            futureMusic = scheduleTaskExecutor.scheduleAtFixedRate(Runnable { val task = RecognizeFromMic()
                task.doFingerprint() }, 0, 30, TimeUnit.SECONDS)

        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun recognizeCommercial(){

// This schedule a runnable task
        futureCommercial = scheduleTaskExecutor.scheduleAtFixedRate({

            run() {
                checkIcyMetadata();
            }
        }, 0, 20, TimeUnit.SECONDS);
    }


    fun checkIcyMetadata() {


        if (nowOnAir != null) {
            var mmr = FFmpegMediaMetadataRetriever();
            mmr.setDataSource(nowOnAir);
            var icy = mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ICY_METADATA);
            mmr.release();

            if (icy != null) {
                val spotSplit = icy.split("-".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                val commercialSplit = spotSplit[0].split("=")

                if (commercialSplit.size > 1) {
                    if (commercialName!= null){
                        artistName=commercialName!!
                    }

                    commercialName = commercialSplit[1]

                }
                if (commercialName?.compareTo("'Sconosciuto ", ignoreCase = true) == 0) {
                    //view.showcommercial
                    val spotProd = spotSplit[1].split(" ")
                    val codiceProdotto = spotProd[3]
                    var array=ArrayList<String>()
                    array.add(artistName!!)
                    array.add(codiceProdotto)
                    showCommercial.postValue(array)
                }else{
                    artistName=commercialName!!.substring(1)

                }
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stopMusicRecognition(){
        futureMusic?.cancel(true)
        futureCommercial?.cancel(true)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun shutDown(){
        scheduleTaskExecutor.shutdownNow()
    }

    internal inner class RecognizeFromMic : GNSearchResultReady {

        fun doFingerprint() {
            println("doFP")
            GNOperations.recognizeMIDStreamFromMic(this, config)
        }

        override fun GNResultReady(result: GNSearchResult?) {
            println("result")
            if (result!!.isFingerprintSearchNoMatchStatus) {
            } else {
                //				for (GNSearchResponse response : result.getResponses()) {
                //					artist_label.setText(response.getArtist());
                //				}
                val response = result.bestResponse
                println("else $response***")
                if (response != null) {
                    Log.i("RECOGNITION OK", "CHECKED!")
                    if(!response.trackTitle.equals(trackTitle, ignoreCase = true)){
                        trackTitle = response.trackTitle
                        val handlerThread = HandlerThread("MyHandlerThread")
                        handlerThread.start()
                        val looper = handlerThread.looper
                        val handler = Handler(looper)
                        handler.post{
                            val imageUrl = SearchClient.search(response.artist)
                            showInfoDialog.postValue(MusicInfo(trackTitle!!,response.artist,imageUrl))
                            artist.postValue(artistName)
                        }
                    }
                } else {
                    //Toast.makeText(this@MainActivity, "nessun risultato", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

}