package ns12.test.testradio.music_recognition.shoutcast

import java.io.IOException
import java.net.URL
import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * Created by userns12 on 12/6/17.
 */
class IcyStream(private val streamUrl: URL) {


    private var metadata: Map<String, String>? = null
    var isError: Boolean = false
        private set

    /**
     * Get artist using stream's title
     *
     * @return String
     * @throws IOException
     */
    val artist: String
        @Throws(IOException::class)
        get() {
            val data = getMetadata()

            if (!data!!.containsKey("StreamTitle"))
                return ""

            val streamTitle = data["StreamTitle"]
            val title = streamTitle?.substring(0, streamTitle.indexOf("-"))
            return title!!.trim { it <= ' ' }
        }

    /**
     * Get title using stream's title
     *
     * @return String
     * @throws IOException
     */
    val title: String
        @Throws(IOException::class)
        get() {
            val data = getMetadata()

            if (!data!!.containsKey("StreamTitle"))
                return ""

            val streamTitle = data["StreamTitle"]
            val artist = streamTitle?.substring(streamTitle.indexOf("-") + 1)
            return artist!!.trim { it <= ' ' }
        }

    init {
        this.metadata = null
        this.isError = false

        isError = false
    }

    @Throws(IOException::class)
    fun getMetadata(): Map<String, String>? {
        if (metadata == null) {
            refreshMeta()
        }

        return metadata
    }

    @Throws(IOException::class)
    fun refreshMeta() {
        retreiveMetadata()
    }

    @Throws(IOException::class)
    private fun retreiveMetadata() {
        val con = streamUrl.openConnection()
        con.setRequestProperty("Icy-MetaData", "1")
        con.setRequestProperty("Connection", "close")
        con.setRequestProperty("Accept", null)
        con.connect()

        var metaDataOffset = 0
        val headers = con.getHeaderFields()
        val stream = con.getInputStream()

        if (headers.containsKey("icy-metaint")) {
            // Headers are sent via HTTP
            metaDataOffset = Integer.parseInt(headers.get("icy-metaint")?.get(0))
        } else {
            // Headers are sent within a stream
            val strHeaders = StringBuilder()
            var c : Char? = null
            while ( {c = stream.read().toChar(); c }()?.toInt() != -1) {
                strHeaders.append(c)
                if (strHeaders.length > 5 && strHeaders.substring(strHeaders.length - 4, strHeaders.length) == "\r\n\r\n") {
                    // end of headers
                    break
                }
            }

            // Match headers to get metadata offset within a stream
            val p = Pattern.compile("\\r\\n(icy-metaint):\\s*(.*)\\r\\n")
            val m = p.matcher(strHeaders.toString())
            if (m.find()) {
                metaDataOffset = Integer.parseInt(m.group(2))
            }
        }

        // In case no data was sent
        if (metaDataOffset == 0) {
            isError = true
            return
        }

        // Read metadata
         var b: Int? = null
        var count = 0
        var metaDataLength = 4080 // 4080 is the max length
        var inData = false
        val metaData = StringBuilder()
        // Stream position should be either at the beginning or right after headers
        while ({b = stream.read();b}() != -1) {
            count++

            // Length of the metadata
            if (count == metaDataOffset + 1) {
                metaDataLength = if (b != null)  b!! * 16   else 0
            }

            if (count > metaDataOffset + 1 && count < metaDataOffset + metaDataLength) {
                inData = true
            } else {
                inData = false
            }
            if (inData) {
                if (b != 0) {
                    metaData.append(b?.toChar())
                }
            }
            if (count > metaDataOffset + metaDataLength) {
                break
            }

        }

        // Set the data
        metadata = IcyStream.parseMetadata(metaData.toString())

        // Close
        stream.close()
    }

    fun getStreamUrl(): URL {
        return streamUrl
    }



    companion object {

        fun parseMetadata(metaString: String): Map<String, String> {
            val metadata = HashMap<String,String>()
            val metaParts = metaString.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val p = Pattern.compile("^([a-zA-Z]+)=\\'([^\\']*)\\'$")
            var m: Matcher
            for (i in metaParts.indices) {
                m = p.matcher(metaParts[i])
                if (m.find()) {
                    metadata.put(m.group(1) as String, m.group(2) as String)
                }
            }

            return metadata
        }
    }
}