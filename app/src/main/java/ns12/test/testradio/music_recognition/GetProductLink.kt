package ns12.test.testradio.music_recognition

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GetProductLink {


    @GET("novelli/select/liban.php")
    fun getProductFromCode(@Query ("cod_prodotto") codice : String, @Query("cod_radio") radio : String) : Call<String>
}