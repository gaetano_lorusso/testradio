package ns12.test.testradio.util;

import org.jetbrains.annotations.Nullable;

/**
 * Created by userns12 on 11/24/17.
 */

public class Constants {

    public static final String URL_RADIO_STREAM_KEY = "URL_RADIO_STREAM_KEY";
    public static  final String URL_IMAGE_RADIO_KEY ="UrlImmagine";

    /***GRACENOTE OPTION ***/
    public static final String clientID = "527200373"; // Put your clientID here.
    public static final String clientTag = "50DB49AFC399181D73D3F13F45C8924F"; // Put your clientTag here.
    @Nullable
    public static final String Play = "play";
    public static final String START_PROGRESS = "START_PROGRESS";
    public static final String PlayFirst ="playFirst";
    public static final String STOP_PROGRESS= "STOP_PROGRESS";
    public static final String KEY_ARGS_LABEL_RADIO = "KEY_ARGS_LABEL_RADIO";
    public static final String KEY_SONG_NAME = "KEY_SONG_NAME";
    public static final String KEY_ARTIST_NAME = "KEY_ARTIST_NAME";
    public static final String KEY_IMAGE_URL_ARTIST = "KEY_IMAGE_URL_ARTIST";
    @Nullable
    public static final String CUSTOM_SEARCH_API_KEY = "AIzaSyC4iayy5l0W3WwzMRsRxscU2htOiNzZ-T0";
    public static final String SEARCH_ENGINE_ID = "015280540634168536848:7gl-n7rxl5g";
    public static final String KEY_URL_WEB_FRAGMENT = "KEY_URL_WEB_FRAGMENT";

    public static final String KEY_SOURCE_RADIO = "KEY_SOURCE_RADIO";
    public static final String KEY_SOURCE_SETTINGS ="KEY_SOURCE_SETTINGS" ;
    public static final String KEY_CITY_SETTINGS ="KEY_CITY_SETTINGS";
    public static final String KEY_HEIGHT_TOOLBAR = "KEY_HEIGHT_TOOLBAR";
    public static final String KEY_PRODUCT = "KEY_PRODUCT";
    public static final String VALUE_SETTINGS_WEB = "WEB";

    public static final int RTL2832U_RESULT_CODE = 1234;
    public static final String KEY_CITY ="KEY_CITY" ;

    public static final String VALUE_DEFAULT_CITY="Roma";
    public static final String SOURCE_ERROR_COMMAND="RELOAD";

    public static final String ERROR_PLAYER = "ERROR_PLAYER";



    public static final String KEY_YOUTUBE="AIzaSyC4iayy5l0W3WwzMRsRxscU2htOiNzZ-T0";

    public static final int RC_SIGN_IN =123;
    public static final String ACCOUNT_SETTINGS = "ACCOUNT_SETTINGS";
    public static final String KEY_YOUTUBE_SETTINGS = "KEY_YOUTUBE_SETTINGS";
    public static final String VALUE_SETTINGS_YOUTUBE = "Lista di Video";

    public static final String FRAGMENT_LIST="FRAGMENT_LIST";

    public static final String FRAGMENT_CONTROL="FRAGMENT_CONTROL";

    public static final String FRAGMENT_YOUTUBE="FRAGMENT_YOUTUBE";
    @Nullable
    public static final String FRAGMENT_COMMERCIAL="FRAGMENT_COMMERCIAL";
    public static final String KEY_CODICE_RADIO = "COD_RADIO";

}
