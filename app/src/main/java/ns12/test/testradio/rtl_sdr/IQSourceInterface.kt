package ns12.test.testradio.Rtl_sdr

import android.content.Context
import ns12.test.testradio.rtl_sdr.SamplePacket

/**
 * Created by userns12 on 12/15/17.
 */
interface IQSourceInterface {


    /**
    * Will open the device. This is usually an asynchronous action and therefore uses the
    * callback function onIQSourceReady() from the Callback
    interface to notify the application
    * when the IQSource is ready to use.
    *
    * @param context needed to open external devices
    * @param callback reference to a
    class that implements the Callback
    interface for notification
    * @ return false if an error occurred.
    */
    abstract fun open (context : Context, callback : Callback): Boolean;

    /**
     * Will return true if the source is opened and ready to use
     *
     * @return true if open, false if not open
     */
    abstract fun isOpen(): Boolean

    /**
     * Will close the device.
     *
     * @return false if an error occurred.
     */
    abstract fun close(): Boolean

    /**
     * @return a human readable Name of the source
     */
    abstract fun getName(): String

    /**
     * @return the rate at which this source receives samples
     */
    abstract fun getSampleRate(): Int

    /**
     * @param sampleRate    sample rate that should be set for the IQ source
     */
    abstract fun setSampleRate(sampleRate: Int)

    /**
     * @return the Frequency to which the source is tuned
     */
    abstract fun getFrequency(): Long

    /**
     * @param frequency        frequency to which the IQ source should be tuned
     */
    abstract fun setFrequency(frequency: Long)


    /**
     * @return the maximum frequency to which the source can be tuned
     */
    abstract fun getMaxFrequency(): Long

    /**
     * @return the minimum frequency to which the source can be tuned
     */
    abstract fun getMinFrequency(): Long

    /**
     * @return the maximum sample rate to which the source can be set
     */
    abstract fun getMaxSampleRate(): Int

    /**
     * @return the minimum sample rate to which the source can be set
     */
    abstract fun getMinSampleRate(): Int

    /**
     * @param sampleRate    initial sample rate for the lookup
     * @return next optimal sample rate that is higher than sampleRate
     */
    abstract fun getNextHigherOptimalSampleRate(sampleRate: Int): Int

    /**
     * @param sampleRate    initial sample rate for the lookup
     * @return next optimal sample rate that is lower than sampleRate
     */
    abstract fun getNextLowerOptimalSampleRate(sampleRate: Int): Int

    /**
     * @return Array of all supported (optimal) sample rates
     */
    abstract fun getSupportedSampleRates(): IntArray

    /**
     * @return the size (in byte) of a packet that is returned by getPacket()
     */
    abstract fun getPacketSize(): Int

    /**
     * This method will grab the next packet from the source and return it. If no
     * packet is available after the timeout, null is returned. Make sure to return
     * the packet to the buffer pool by using returnPacket() after it is no longer used.
     *
     * @return packet containing received samples
     */
    abstract fun getPacket(timeout: Int): ByteArray

    /**
     * This method will return the given buffer (packet) to the buffer pool of the
     * source instance.
     *
     * @param buffer    A packet that was returned by getPacket() and is now no longer used
     */
    abstract fun returnPacket(buffer: ByteArray)

    /**
     * Start receiving samples.
     */
    abstract fun startSampling()

    /**
     * Stop receiving samples.
     */
    abstract fun stopSampling()

    /**
     * Used to convert a packet from this source to the SamplePacket format. That means the samples
     * in the SamplePacket are stored as signed double values, normalized between -1 and 1.
     * Note that samples are appended to the buffer starting at the index samplePacket.size().
     * If you want to overwrite, set the size to 0 first.
     *
     * @param packet        packet that was returned by getPacket() and that should now be 'filled'
     * into the samplePacket.
     * @param samplePacket    SamplePacket that should be filled with samples from the packet.
     * @return the number of samples filled into the samplePacket.
     */
    abstract fun fillPacketIntoSamplePacket(packet: ByteArray, samplePacket: SamplePacket): Int

    /**
     * Used to convert a packet from this source to the SamplePacket format while at the same
     * time mixing the signal with the specified frequency. That means the samples
     * in the SamplePacket are stored as signed double values, normalized between -1 and 1.
     * Note that samples are appended to the buffer starting at the index samplePacket.size().
     * If you want to overwrite, set the size to 0 first.
     *
     * @param packet            packet that was returned by getPacket() and that should now be 'filled'
     * into the samplePacket.
     * @param samplePacket        SamplePacket that should be filled with samples from the packet.
     * @param channelFrequency    frequency to which the spectrum of the signal should be shifted
     * @return the number of samples filled into the samplePacket and shifted by mixFrequency.
     */
    abstract fun mixPacketIntoSamplePacket(packet: ByteArray, samplePacket: SamplePacket, channelFrequency: Long): Int

    /**
     * Callback interface for asynchronous interactions with the source.
     */
    interface Callback {
        /**
         * This method will be called when the source is ready to use after the application
         * called open()
         *
         * @param source    A reference to the IQSource that is now ready
         */
        fun onIQSourceReady(source: IQSourceInterface)

        /**
         * This method will be called when there is an error with the source
         *
         * @param source    A reference to the IQSource that caused the error
         * @param message    Description of the error
         */
        fun onIQSourceError(source: IQSourceInterface, message: String)


        fun onIQSoureReload(source: IQSourceInterface,message: String)
    }

}