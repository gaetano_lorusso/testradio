package ns12.test.testradio.rtl_sdr;

/**
 * Created by userns12 on 12/15/17.
 */
class FFT(n : Int) {

    internal var n: Int = n
    internal var m:Int = 0

    // Lookup tables.  Only need to recompute when size of FFT changes.
    internal lateinit var cos: FloatArray
    internal lateinit var sin: FloatArray

    internal lateinit var window: FloatArray

    init{
        this.n = n
        this.m = (Math.log(n.toDouble()) / Math.log(2.0)).toInt()

        // Make sure n is a power of 2
        if (n != 1 shl m)
            throw RuntimeException("FFT length must be power of 2")

        // precompute tables
        cos = FloatArray(n / 2)
        sin = FloatArray(n / 2)

        //     for(int i=0; i<n/4; i++) {
        //       cos[i] = Math.cos(-2*Math.PI*i/n);
        //       sin[n/4-i] = cos[i];
        //       cos[n/2-i] = -cos[i];
        //       sin[n/4+i] = cos[i];
        //       cos[n/2+i] = -cos[i];
        //       sin[n*3/4-i] = -cos[i];
        //       cos[n-i]   = cos[i];
        //       sin[n*3/4+i] = -cos[i];
        //     }

        for (i in 0 until n / 2) {
            cos[i] = Math.cos(-2.0 * Math.PI * i.toDouble() / n).toFloat()
            sin[i] = Math.sin(-2.0 * Math.PI * i.toDouble() / n).toFloat()
        }

        makeWindow()
    }

    protected fun makeWindow() {
        // Make a blackman window:
        // w(n)=0.42-0.5cos{(2*PI*n)/(N-1)}+0.08cos{(4*PI*n)/(N-1)};
        window = FloatArray(n)
        for (i in window.indices)
            window[i] = (0.42 - 0.5 * Math.cos(2.0 * Math.PI * i.toDouble() / (n - 1)) + 0.08 * Math.cos(4.0 * Math.PI * i.toDouble() / (n - 1))).toFloat()
    }

    fun getWindow(): FloatArray {
        return window
    }

    fun applyWindow(re: FloatArray, im: FloatArray) {
        for (i in window.indices) {
            re[i] = window[i] * re[i]
            im[i] = window[i] * im[i]
        }
    }


    /***************************************************************
     * fft.c
     * Douglas L. Jones
     * University of Illinois at Urbana-Champaign
     * January 19, 1992
     * http://cnx.rice.edu/content/m12016/latest/
     *
     * fft: in-place radix-2 DIT DFT of a complex input
     *
     * input:
     * n: length of FFT: must be a power of two
     * m: n = 2**m
     * input/output
     * x: double array of length n with real part of data
     * y: double array of length n with imag part of data
     *
     * Permission to copy and use this program is granted
     * as long as this header is included.
     */
    fun fft(x: FloatArray, y: FloatArray) {
        var i: Int
        var j: Int
        var k: Int
        var n1: Int
        var n2: Int
        var a: Int
        var c: Float
        var s: Float
        val e: Float
        var t1: Float
        var t2: Float


        // Bit-reverse
        j = 0
        n2 = n / 2
        i = 1
        while (i < n - 1) {
            n1 = n2
            while (j >= n1) {
                j = j - n1
                n1 = n1 / 2
            }
            j = j + n1

            if (i < j) {
                t1 = x[i]
                x[i] = x[j]
                x[j] = t1
                t1 = y[i]
                y[i] = y[j]
                y[j] = t1
            }
            i++
        }

        // FFT
        n1 = 0
        n2 = 1

        i = 0
        while (i < m) {
            n1 = n2
            n2 = n2 + n2
            a = 0

            j = 0
            while (j < n1) {
                c = cos[a]
                s = sin[a]
                a += 1 shl m - i - 1

                k = j
                while (k < n) {
                    t1 = c * x[k + n1] - s * y[k + n1]
                    t2 = s * x[k + n1] + c * y[k + n1]
                    x[k + n1] = x[k] - t1
                    y[k + n1] = y[k] - t2
                    x[k] = x[k] + t1
                    y[k] = y[k] + t2
                    k = k + n2
                }
                j++
            }
            i++
        }
    }


}