package ns12.test.testradio

import android.app.Application
import android.os.StrictMode
import ns12.test.testradio.Repository.RadioDatabase


/**
 * Created by userns12 on 1/4/18.
 */
class RadioNovelliApp : Application() {


    companion object {
        lateinit var instance : RadioNovelliApp private set

    }



    override fun onCreate() {
        super.onCreate()
        instance = this
        RadioDatabase.getInstance(this)
        StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build())
    }




}