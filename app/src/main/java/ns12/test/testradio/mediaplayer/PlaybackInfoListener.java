package ns12.test.testradio.mediaplayer;

import android.support.v4.media.session.PlaybackStateCompat;

/**
 * Created by userns12 on 11/24/17.
 */

public abstract class PlaybackInfoListener {

    public abstract void onPlaybackStateChange(PlaybackStateCompat state);

    public void onPlaybackCompleted() {
    }
}
