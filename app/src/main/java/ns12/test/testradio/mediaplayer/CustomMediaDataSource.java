package ns12.test.testradio.mediaplayer;

import android.media.MediaDataSource;

import java.io.IOException;

/**
 * Created by userns12 on 12/18/17.
 */

public class CustomMediaDataSource extends MediaDataSource {

    private volatile byte[] audioSource;




    @Override
    public int readAt(long l, byte[] bytes, int i, int i1) throws IOException {
        return 0;
    }

    @Override
    public long getSize() throws IOException {
        return audioSource.length;
    }

    @Override
    public void close() throws IOException {

    }
}
