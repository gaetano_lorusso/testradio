package ns12.test.testradio.mediaplayer;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaBrowserServiceCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ns12.test.testradio.util.Constants;

import static android.support.v4.media.MediaBrowserCompat.MediaItem.FLAG_PLAYABLE;


/**
 * Created by userns12 on 11/24/17.
 */

public class ServiceAudioStreaming extends MediaBrowserServiceCompat {


    private static final String MY_MEDIA_ROOT_ID = "media_root_id";
    private static final java.lang.String LOG_TAG = "AUDIO SERVICE";
    private MediaSessionCompat mMediaSession;
    private PlaybackStateCompat.Builder mStateBuilder;
    private PlayerAdapter mPlayback;
    private Uri radioUri;
    private boolean mServiceInStartedState;
    //private MediaNotificationManager mMediaNotificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mMediaSession = new MediaSessionCompat(this, LOG_TAG);

        // Enable callbacks from MediaButtons and TransportControls
        mMediaSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                        MediaSessionCompat.FLAG_HANDLES_QUEUE_COMMANDS |
                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS );

        // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player
        mStateBuilder = new PlaybackStateCompat.Builder()
                .setActions(
                        PlaybackStateCompat.ACTION_PLAY |
                                PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_PLAY_FROM_URI | PlaybackStateCompat.ACTION_PREPARE_FROM_URI);
        mMediaSession.setPlaybackState(mStateBuilder.build());

        // MySessionCallback() has methods that handle callbacks from a media controller
        mMediaSession.setCallback(new MediaSessionCallback());

        // Set the session's token so that client activities can communicate with it.
        setSessionToken(mMediaSession.getSessionToken());
        mMediaSession.setActive(true);

        mPlayback = new MediaPlayerAdapter(this,new MediaPlayerListener(),mMediaSession);
    }




    public class MediaSessionCallback extends MediaSessionCompat.Callback {
        private final List<MediaSessionCompat.QueueItem> mPlaylist = new ArrayList<>();
        private int mQueueIndex = -1;
        private MediaMetadataCompat mPreparedMedia;

        /**
        NOT USED
         */
        @Override
        public void onAddQueueItem(MediaDescriptionCompat description, int index) {
            mPlaylist.clear();
            mPreparedMedia = null;
            mPlaylist.add(0,new MediaSessionCompat.QueueItem(description, description.hashCode()));
            mQueueIndex = (mQueueIndex == -1) ? 0 : mQueueIndex;
            mMediaSession.sendSessionEvent(Constants.Play,null);
        }

        /**
         NOT USED
         */
        @Override
        public void onRemoveQueueItem(MediaDescriptionCompat description) {
            mPlaylist.remove(new MediaSessionCompat.QueueItem(description, description.hashCode()));
            mQueueIndex = (mPlaylist.isEmpty()) ? -1 : mQueueIndex;
        }


        /**
         NOT USED
         */
        @Override
        public void onPrepare() {

                MediaMetadataCompat.Builder builder = new MediaMetadataCompat.Builder();
                builder.putString(MediaMetadataCompat.METADATA_KEY_MEDIA_URI, radioUri.toString());
                mPreparedMedia = builder.build();
                mMediaSession.setMetadata(mPreparedMedia);

                if (!mMediaSession.isActive()) {
                    mMediaSession.setActive(true);
                }
        }






        @Override
        public void onPause() {
            mPlayback.pause();
        }

        @Override
        public void onStop() {
            mPlayback.stop();
            mMediaSession.setActive(false);
        }

        @Override
        public void onSkipToNext() {
            mQueueIndex = (++mQueueIndex % mPlaylist.size());
            mPreparedMedia = null;
            onPlay();
        }

        @Override
        public void onSkipToPrevious() {
            mQueueIndex = mQueueIndex > 0 ? mQueueIndex - 1 : mPlaylist.size() - 1;
            mPreparedMedia = null;
            onPlay();
        }

        @Override
        public void onSeekTo(long pos) {
            mPlayback.seekTo(pos);
        }

        private boolean isReadyToPlay() {
            return (!mPlaylist.isEmpty());
        }

        @Override
        public void onPlayFromUri(Uri uri, Bundle extras) {



            mPlayback.playFromUri(uri);
            if (!mMediaSession.isActive()) {
                mMediaSession.setActive(true);
            }
            Log.d("WEB_RADIO", "onPlayFromMediaId: MediaSession active");
        }



    }




    @Override
    public void notifyChildrenChanged(@NonNull String parentId, @NonNull Bundle options) {
        super.notifyChildrenChanged(parentId, options);
    }

    @Nullable
    @Override
    public BrowserRoot onGetRoot(@NonNull String s, int i, @Nullable Bundle bundle) {
        return new BrowserRoot(MY_MEDIA_ROOT_ID, null);
    }

    @Override
    public void onLoadChildren(@NonNull String parentId, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {
        List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList<>();
        MediaDescriptionCompat descriptionCompat = new MediaDescriptionCompat.Builder().setMediaId("test").build();
        MediaBrowserCompat.MediaItem item = new MediaBrowserCompat.MediaItem(descriptionCompat,FLAG_PLAYABLE);
        mediaItems.add(item);
        result.sendResult(mediaItems);
    }

    @Override
    public void onLoadItem(String itemId, @NonNull Result<MediaBrowserCompat.MediaItem> result) {
        super.onLoadItem(itemId, result);
    }



    public class MediaPlayerListener extends PlaybackInfoListener {

        private final ServiceManager mServiceManager;

        MediaPlayerListener() {
            mServiceManager = new ServiceManager();
        }

        @Override
        public void onPlaybackStateChange(PlaybackStateCompat state) {
            // Report the state to the MediaSession.
            mMediaSession.setPlaybackState(state);

            // Manage the started state of this service.
            switch (state.getState()) {
                case PlaybackStateCompat.STATE_PLAYING:
                    mServiceManager.moveServiceToStartedState(state);
                    break;
                case PlaybackStateCompat.STATE_PAUSED:
                    break;
                case PlaybackStateCompat.STATE_STOPPED:
                    mServiceManager.moveServiceOutOfStartedState(state);
                    mMediaSession.sendSessionEvent(Constants.STOP_PROGRESS,null);
                    break;
                case PlaybackStateCompat.ERROR_CODE_ACTION_ABORTED:
                    mMediaSession.sendSessionEvent(Constants.ERROR_PLAYER,null);
            }
        }



        class ServiceManager {

            private void moveServiceToStartedState(PlaybackStateCompat state) {
                if (!mServiceInStartedState) {
                    startService(new Intent(ServiceAudioStreaming.this, ServiceAudioStreaming.class));
                    mServiceInStartedState = true;
                }
            }

            private void updateNotificationForPause(PlaybackStateCompat state) {

            }

            private void moveServiceOutOfStartedState(PlaybackStateCompat state) {
                stopForeground(true);
                stopSelf();
                mServiceInStartedState = false;
            }
        }

    }


}
