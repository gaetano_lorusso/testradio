package ns12.test.testradio.model


import javax.xml.transform.Source

/**
 * Created by userns12 on 1/5/18.
 */
data class MainModel(var connectionState : String, var currentSource : Source, var currentRadio : Radio, var showCommercialButton: Boolean,
                     var showMusicRecognition : Boolean) {



}