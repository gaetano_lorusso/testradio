package ns12.test.testradio.model;

public class Radio {

    private String url;
    private String immagine;
    private String nome;
    private String codice;
    private long frequency;

    public Radio(String nome,String web,String imageurl){
        this.nome=nome;
        this.url=web;
        this.immagine=imageurl;
        this.frequency=0;
        this.codice = "";

    }

    public Radio(String nome,String web,String imageurl, String codice){
        this.nome=nome;
        this.url=web;
        this.immagine=imageurl;
        this.frequency=0;
        this.codice = codice;

    }

    public String getNome(){
        return this.nome;
    }

    public String getUrl(){
        return this.url;
    }

    public String getImmagine(){
        return this.immagine;
    }

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public long getFrequency() {
        return frequency;
    }

    public Radio setFrequency(String frequency) {
        if(frequency.isEmpty())
            this.frequency = 0;
        else

            this.frequency = Long.parseLong(frequency);
        return this;
    }
}

