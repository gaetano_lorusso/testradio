package ns12.test.testradio.model

/**
 * Created by userns12 on 1/5/18.
 */
data class MusicInfo(val title : String, val artist : String, val imageUrl : String)